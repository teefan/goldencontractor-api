# frozen_string_literal: true
source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

# Use dotenv to read environment variables from .env file
gem 'dotenv-rails'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.0.1'
gem 'rails-i18n', '~> 5.0.0'
# Use ActiveModel serializer to build JSON response
gem 'active_model_serializers', '~> 0.10.0'
# A set of responders modules to dry up app.
gem 'responders'
# Use postgresql as the database for Active Record
gem 'pg', '~> 0.18'
# Use Puma as the app server
gem 'puma', '~> 3.0'
# Use Oj gem for fast JSON parsing
gem 'oj'
# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 3.0'
# Use Sidekiq for background jobs processing
gem 'sidekiq'
# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'

# Use Carrierwave for file upload
gem 'carrierwave', '~> 1.0'
# Use MiniMagick as wrapper for ImageMagick
gem 'mini_magick'
# Use Fog for Ruby cloud services
gem 'fog-aws'
gem 'fog-local'

# Use Devise for authentication
gem 'devise'
gem 'devise-i18n'
# Use Pundit for authorization
gem 'pundit'
# Use Rolify for roles management
gem 'rolify'
# Use ActiveDecorator for presentation decoration
gem 'active_decorator'
# Use JSON Web Token
gem 'jwt'
# Use AASM as state machine to handle Model states
gem 'aasm'

# Use Swagger for API documentation
gem 'swagger-blocks'

# Use Kaminari for pagination
gem 'kaminari'
# API pagination by headers
gem 'api-pagination'

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem 'rack-cors'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platform: :mri
end

group :development do
  # Use Bullet to detect N+1 queries
  gem 'bullet'

  # Annotate model schema
  gem 'annotate'
  # Use FFaker to generate test data
  gem 'ffaker'

  gem 'overcommit', require: false
  gem 'rubocop', '~> 0.46.0', require: false
  gem 'rubycritic', require: false

  gem 'guard'
  gem 'guard-minitest'
  gem 'guard-rubocop'

  # Use Capistrano for deployment
  gem 'capistrano', '~> 3.6'
  gem 'capistrano-bundler'
  gem 'capistrano-passenger'
  gem 'capistrano-rails', '~> 1.2'
  gem 'capistrano-rvm'
end

group :test do
  gem 'minitest-reporters', require: false
  gem 'rack-test', require: 'rack/test'
  gem 'ruby-prof'
  gem 'simplecov', require: false
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
