# Golden Contractor 3.0 REST API

This README document whatever steps are necessary to get the
application up and running.

Important: this project uses `overcommit` and `guard` to make sure consistency of code styling.

It is highly recommended that you run `overcommit --install` after first gem installation, and `bundle exec guard`
during development.

This is a `Profile`-based application. Once everything is configured, you could start the app by running `foreman s`,
short-cut for `foreman start`. This will spin-up all required services and the app itself.

Swagger API definition is located at: https://api.nhathauvang.com/api_doc.json

API documents site (using Swagger UI) is located at: https://api.nhathauvang.com

* Ruby version: 2.3.3

* System dependencies: PostgresSQL, Redis, MailCatcher (development)

* Configuration: `.env`, `.ruby-version`, `.ruby-gemset`

  `.env` file:  
  RAILS_ENV=development  
  RACK_ENV=development  

  HOST=nhathauvang.dev  
  PORT=3000  

  DEVISE_PEPPER=  

  AWS_ACCESS_KEY_ID=  
  AWS_SECRET_ACCESS_KEY=  

  FOG_HOST=  
  FOG_REGION=  
  FOG_ENDPOINT=  
  FOG_DIRECTORY=  

  ROOT_EMAIL=  
  ROOT_PASSWORD=  

* Database creation: `rails db:create`

* Database initialization: `rails db:schema:load`, `rails db:seed`

* How to run the test suite: `rails test`

* Services (job queues, cache servers, search engines, etc.)

  - Job queues: using Sidekiq and **whenever** gem for scheduled cron jobs.
  - Search engine: using Apache Solr search engine via Sunspot gem.

* Deployment instructions: using Capistrano for deployment

  - Run `cap production deploy` or `cap staging deploy` to deploy.
  - Run `ssh-add`, `ssh-agent` to add your private key if required (deploy failed).
