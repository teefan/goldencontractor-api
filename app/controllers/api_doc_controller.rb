# frozen_string_literal: true
# rubocop:disable BlockLength, ClassLength
class APIDocController < ApplicationController
  # A list of all classes that have swagger_* declarations.
  SWAGGERED_CLASSES = [
    V1::CountriesController,
    V1::CitiesController,
    V1::DistrictsController,
    V1::Users::RegistrationsController,
    V1::UsersController,
    V1::RolesController,
    V1::UserRolesController,
    V1::UserProfilesController,
    V1::CompaniesController,
    V1::ConstructionServicesController,
    V1::ConstructionTypesController,
    V1::ArchitectsController,
    V1::JobsController,
    V1::AttachmentsController,
    V1::BidsController,
    Country,
    City,
    District,
    User,
    UserProfile,
    Role,
    Company,
    ConstructionService,
    ConstructionType,
    Architect,
    Job,
    Attachment,
    Bid,
    ErrorModel,
    self
  ].freeze

  include Swagger::Blocks

  swagger_root do
    key :swagger, '2.0'

    info do
      key :version, '0.0.1'
      key :title, 'GoldenContractor 3.0 REST API'
      key :description, 'REST API in JSON for GoldenContractor applications.<br><br>' \
                        'Accept header must include required format and version: ' \
                        '<strong>Accept: application/json; version=1</strong><br><br>' \
                        'Auth token must be included in Authorization header to access restricted resources:<br>' \
                        '<strong>Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...</strong><br>'
      key :termsOfService, 'http://nhathauvang.dev/terms/'
      contact do
        key :name, 'GoldenContractor API Team - teefan@me.com'
      end
    end

    tag do
      key :name, 'account'
      key :description, 'User account & profile management'
    end

    tag do
      key :name, 'session'
      key :description, 'User session authentication'
    end

    tag do
      key :name, 'password'
      key :description, 'User password management'
    end

    tag do
      key :name, 'user'
      key :description, 'User information & listing'
    end

    tag do
      key :name, 'user_profile'
      key :description, 'User profile information'
    end

    tag do
      key :name, 'role'
      key :description, 'Available system roles management'
    end

    tag do
      key :name, 'user_role'
      key :description, 'User roles management'
    end

    tag do
      key :name, 'country'
      key :description, 'Available system countries'
    end

    tag do
      key :name, 'city'
      key :description, 'Available system cities'
    end

    tag do
      key :name, 'district'
      key :description, 'Available system districts'
    end

    tag do
      key :name, 'company'
      key :description, 'User company information'
    end

    tag do
      key :name, 'construction_service'
      key :description, 'Available system construction services'
    end

    tag do
      key :name, 'construction_type'
      key :description, 'Available system construction types'
    end

    tag do
      key :name, 'architect'
      key :description, 'Architects information'
    end

    tag do
      key :name, 'job'
      key :description, 'Job requests management'
    end

    tag do
      key :name, 'attachment'
      key :description, 'Attachment upload service'
    end

    tag do
      key :name, 'bid'
      key :description, 'Job bidding service'
    end

    key :host, 'api.nhathauvang.dev'
    key :basePath, '/'
    key :consumes, ['application/json']
    key :produces, ['application/json; version=1']

    security_definition :authToken do
      key :type, :apiKey
      key :name, :Authorization
      key :in, :header
      key :description, 'Input JWT auth token. Must include "Bearer" in value field. Example: "Bearer 1e2a630cb1..."'
    end
  end

  def index
    render json: Swagger::Blocks.build_root_json(SWAGGERED_CLASSES)
  end
end
