# frozen_string_literal: true
class ApplicationController < ActionController::API
  include Pundit
  include ActionController::MimeResponds
  include CustomErrorRescue
  respond_to :json

  before_action :set_locale

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end
end
