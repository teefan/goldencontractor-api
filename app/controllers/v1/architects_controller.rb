# frozen_string_literal: true
module V1
  class ArchitectsController < ApplicationController
    include ArchitectsPathDocument

    before_action :authenticate_user!
    before_action :set_architect, only: [:show, :update, :destroy]

    # GET /architects
    def index
      @architects = paginate(Architect.filter(filtering_params))

      respond_with @architects
    end

    # GET /architects/1
    def show
      respond_with @architect
    end

    # POST /architects
    def create
      @architect = Architect.new(architect_params)

      @architect.save

      respond_with @architect
    end

    # PATCH/PUT /architects/1
    def update
      @architect.update(architect_params)

      respond_with @architect
    end

    # DELETE /architects/1
    def destroy
      @architect.destroy
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_architect
      @architect = Architect.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def architect_params
      params.require(:architect).permit(:name, :description, :email, :phone_number, :address,
                                        :country_id, :city_id, :district_id)
    end

    # A list of the param names that can be used for filtering
    def filtering_params
      params.slice(:email, :country_id, :city_id, :district_id)
    end
  end
end
