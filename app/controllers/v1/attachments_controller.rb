# frozen_string_literal: true
module V1
  class AttachmentsController < ApplicationController
    include AttachmentsPathDocument

    before_action :authenticate_user!, except: [:index, :show, :create]
    before_action :set_attachment, only: [:show, :update, :destroy]

    # GET /attachments
    def index
      @attachments = paginate(Attachment.filter(filtering_params))

      respond_with @attachments
    end

    # GET /attachments/1
    def show
      respond_with @attachment
    end

    # POST /attachments
    def create
      if attachment_params[:files] && attachment_params[:files].count.positive?
        @attachments = handle_multiple_files

        render json: @attachments

        return
      end

      @attachment = Attachment.new(attachment_params)

      @attachment.save

      respond_with @attachment
    end

    # PATCH/PUT /attachments/1
    def update
      authorize @attachment

      @attachment.update(attachment_params)

      respond_with @attachment
    end

    # DELETE /attachments/1
    def destroy
      authorize @attachment

      @attachment.destroy
    end

    private

    def handle_multiple_files
      @attachments = []

      attachment_params.delete :file
      attachment_params[:files].each do |file|
        attachment = Attachment.new(
          file: file,
          attachable_id:   attachment_params[:attachable_id],
          attachable_type: attachment_params[:attachable_type],
          user_id:         attachment_params[:user_id]
        )

        attachment.save!

        @attachments << attachment
      end

      @attachments
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_attachment
      @attachment = Attachment.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def attachment_params
      params.require(:attachment).permit(:file, :file_size, :content_type, :attachable_id, :attachable_type, :user_id,
                                         files: [])
    end

    # A list of the param names that can be used for filtering
    def filtering_params
      params.slice(:attachable_id, :attachable_type, :user_id)
    end
  end
end
