# frozen_string_literal: true
module V1
  class BidsController < ApplicationController
    include BidsPathDocument

    before_action :authenticate_user!
    before_action :set_bid, only: [:show, :update, :destroy]

    # GET /bids
    def index
      @bids = Bid.filter(filtering_params)

      respond_with @bids
    end

    # GET /bids/1
    def show
      respond_with @bid
    end

    # POST /bids
    def create
      @bid = Bid.new(bid_params)

      @bid.save

      respond_with @bid
    end

    # PATCH/PUT /bids/1
    def update
      @bid.update(bid_params)

      respond_with @bid
    end

    # DELETE /bids/1
    def destroy
      @bid.destroy
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_bid
      @bid = Bid.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def bid_params
      params.require(:bid).permit(:job_id, :user_id, :flow_action)
    end

    # A list of the param names that can be used for filtering
    def filtering_params
      params.slice(:job_id, :user_id)
    end
  end
end
