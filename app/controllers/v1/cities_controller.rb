# frozen_string_literal: true
module V1
  class CitiesController < ApplicationController
    include CitiesPathDocument

    before_action :authenticate_user!, except: [:index, :show]
    before_action :set_city, only: [:show, :update, :destroy]

    # GET /cities
    def index
      @cities = City.filter(filtering_params)

      respond_with @cities
    end

    # GET /cities/1
    def show
      respond_with @city
    end

    # POST /cities
    def create
      @city = City.new(city_params)
      authorize @city

      @city.save

      respond_with @city
    end

    # PATCH/PUT /cities/1
    def update
      authorize @city

      @city.update(city_params)

      respond_with @city
    end

    # DELETE /cities/1
    def destroy
      authorize @city

      @city.destroy
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_city
      @city = City.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def city_params
      params.require(:city).permit(:name, :active, :country_id)
    end

    # A list of the param names that can be used for filtering
    def filtering_params
      params.slice(:country_id)
    end
  end
end
