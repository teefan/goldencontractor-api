# frozen_string_literal: true
module V1
  class CompaniesController < ApplicationController
    include CompaniesPathDocument

    before_action :authenticate_user!, except: [:index, :show]
    before_action :set_company, only: [:show, :update, :destroy]

    # GET /companies
    def index
      @companies = paginate(Company.filter(filtering_params))

      respond_with @companies
    end

    # GET /companies/1
    def show
      respond_with @company
    end

    # POST /companies
    def create
      @company = Company.new(company_params)
      @company.ensure_owner_presence(current_user)

      @company.save

      respond_with @company
    end

    # PATCH/PUT /companies/1
    def update
      @company.update(company_params)

      respond_with @company
    end

    # DELETE /companies/1
    def destroy
      @company.destroy
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_company
      @company = Company.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def company_params
      params.require(:company).permit(:name, :tax_number, :street_address, :website_url,
                                      :country_id, :city_id, :district_id, :user_id)
    end

    # A list of the param names that can be used for filtering
    def filtering_params
      params.slice(:tax_number, :country_id, :city_id, :district_id, :user_id)
    end
  end
end
