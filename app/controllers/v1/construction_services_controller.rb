# frozen_string_literal: true
module V1
  class ConstructionServicesController < ApplicationController
    include ConstructionServicesPathDocument

    before_action :authenticate_user!, except: [:index, :show]
    before_action :set_construction_service, only: [:show, :update, :destroy]

    # GET /construction_services
    def index
      @construction_services = ConstructionService.all

      respond_with @construction_services
    end

    # GET /construction_services/1
    def show
      respond_with @construction_service
    end

    # POST /construction_services
    def create
      @construction_service = ConstructionService.new(construction_service_params)

      @construction_service.save

      respond_with @construction_service
    end

    # PATCH/PUT /construction_services/1
    def update
      @construction_service.update(construction_service_params)

      respond_with @construction_service
    end

    # DELETE /construction_services/1
    def destroy
      @construction_service.destroy
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_construction_service
      @construction_service = ConstructionService.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def construction_service_params
      params.require(:construction_service).permit(:name, :active)
    end
  end
end
