# frozen_string_literal: true
module V1
  class ConstructionTypesController < ApplicationController
    include ConstructionTypesPathDocument

    before_action :authenticate_user!, except: [:index, :show]
    before_action :set_construction_type, only: [:show, :update, :destroy]

    # GET /construction_types
    def index
      @construction_types = ConstructionType.all

      respond_with @construction_types
    end

    # GET /construction_types/1
    def show
      respond_with @construction_type
    end

    # POST /construction_types
    def create
      @construction_type = ConstructionType.new(construction_type_params)

      @construction_type.save

      respond_with @construction_type
    end

    # PATCH/PUT /construction_types/1
    def update
      @construction_type.update(construction_type_params)

      respond_with @construction_type
    end

    # DELETE /construction_types/1
    def destroy
      @construction_type.destroy
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_construction_type
      @construction_type = ConstructionType.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def construction_type_params
      params.require(:construction_type).permit(:name, :active)
    end
  end
end
