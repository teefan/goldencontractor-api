# frozen_string_literal: true
module V1
  class CountriesController < ApplicationController
    include CountriesPathDocument

    before_action :authenticate_user!, except: [:index, :show]
    before_action :set_country, only: [:show, :update, :destroy]

    # GET /countries
    def index
      @countries = Country.filter(filtering_params)

      respond_with @countries
    end

    # GET /countries/1
    def show
      respond_with @country
    end

    # POST /countries
    def create
      @country = Country.new(country_params)
      authorize @country

      @country.save

      respond_with @country
    end

    # PATCH/PUT /countries/1
    def update
      authorize @country

      @country.update(country_params)

      respond_with @country
    end

    # DELETE /countries/1
    def destroy
      authorize @country

      @country.destroy
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_country
      @country = Country.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def country_params
      params.require(:country).permit(:name, :alpha2, :active)
    end

    # A list of the param names that can be used for filtering
    def filtering_params
      params.slice(:alpha2)
    end
  end
end
