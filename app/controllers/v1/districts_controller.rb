# frozen_string_literal: true
module V1
  class DistrictsController < ApplicationController
    include DistrictsPathDocument

    before_action :set_district, only: [:show, :update, :destroy]

    # GET /districts
    def index
      @districts = District.filter(filtering_params)

      respond_with @districts
    end

    # GET /districts/1
    def show
      respond_with @district
    end

    # POST /districts
    def create
      @district = District.new(district_params)
      authorize @district

      @district.save

      respond_with @district
    end

    # PATCH/PUT /districts/1
    def update
      authorize @district

      @district.update(district_params)

      respond_with @district
    end

    # DELETE /districts/1
    def destroy
      authorize @district

      @district.destroy
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_district
      @district = District.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def district_params
      params.require(:district).permit(:name, :active, :city_id, :country_id)
    end

    # A list of the param names that can be used for filtering
    def filtering_params
      params.slice(:city_id, :country_id)
    end
  end
end
