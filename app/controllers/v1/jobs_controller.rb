# frozen_string_literal: true
module V1
  class JobsController < ApplicationController
    include JobsPathDocument

    before_action :authenticate_user!, except: [:index, :show, :create]
    before_action :set_job, only: [:show, :update, :destroy]

    # GET /jobs
    def index
      @jobs = paginate(
        Job.includes(:construction_service, :construction_type, :city, :district, :attachments).filter(filtering_params)
      )

      respond_with @jobs
    end

    # GET /jobs/1
    def show
      respond_with @job
    end

    # POST /jobs
    def create
      if job_params[:user_id].blank? && user_signed_in?
        @job = current_user.jobs.new(job_params)
      else
        @job = Job.new(job_params)
        User.create_user(params[:user])
      end

      params[:validate_only].present? ? @job.valid? : @job.save

      respond_with @job
    end

    # PATCH/PUT /jobs/1
    def update
      @job.update(job_params)

      respond_with @job
    end

    # DELETE /jobs/1
    def destroy
      @job.destroy
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_job
      @job = Job.includes(:construction_service, :construction_type, :city, :district, :attachments).find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def job_params
      params.require(:job).permit(:title, :description, :land_width, :land_length, :land_area, :floor_width,
                                  :floor_length, :floor_area, :number_of_floors, :has_terrace, :has_mezzanine,
                                  :has_basement, :planned_start_date, :estimated_cost, :budget_level, :budget,
                                  :verified, :published_at, :construction_service_id, :construction_type_id,
                                  :workflow_status, :next_processing_date, :max_contractors, :address,
                                  :country_id, :city_id, :district_id, :user_id, :flow_action, attachment_ids: [])
    end

    # A list of the param names that can be used for filtering
    def filtering_params
      params.slice(:construction_service_id, :construction_type_id, :country_id, :city_id, :district_id, :user_id)
    end
  end
end
