# frozen_string_literal: true
module V1
  class RolesController < ApplicationController
    include RolesPathDocument

    before_action :authenticate_user!, except: [:index]
    before_action :set_role, only: [:update, :destroy]

    # GET /roles
    def index
      @roles = Role.all

      respond_with @roles
    end

    # POST /roles
    def create
      @role = Role.new(role_params)

      authorize @role

      @role.save

      respond_with @role
    end

    # PATCH/PUT /roles/1
    def update
      authorize @role

      @role.update!(role_params)

      respond_with @role
    end

    # DELETE /roles/1
    def destroy
      authorize @role

      @role.destroy
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_role
      @role = Role.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def role_params
      params.require(:role).permit(:name, :resource_type, :resource_id)
    end
  end
end
