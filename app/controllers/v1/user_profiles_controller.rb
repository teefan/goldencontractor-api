# frozen_string_literal: true
module V1
  class UserProfilesController < ApplicationController
    include UserProfilePathDocument

    before_action :authenticate_user!
    before_action :set_user_profile, only: [:index, :update, :destroy]

    # GET /user_profile
    def index
      respond_with @user_profile
    end

    # GET /user_profiles/1
    def show
      @user_profile = UserProfile.find(params[:id])

      respond_with @user_profile, location: user_profiles_url(@user_profile)
    end

    # POST /user_profile
    def create
      @user_profile = current_user.build_user_profile(user_profile_params)

      @user_profile.save

      respond_with @user_profile
    end

    # PATCH/PUT /user_profile
    def update
      @user_profile.update(user_profile_params)

      respond_with @user_profile
    end

    # DELETE /user_profile
    def destroy
      @user_profile.destroy
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_user_profile
      @user_profile = current_user.user_profile
    end

    # Only allow a trusted parameter "white list" through.
    def user_profile_params
      params.require(:user_profile).permit(:name, :gender, :date_of_birth, :phone_number, :street_address, :avatar,
                                           :country_id, :city_id, :district_id)
    end
  end
end
