# frozen_string_literal: true
module V1
  class UserRolesController < ApplicationController
    include UserRolesPathDocument

    before_action :authenticate_user!
    before_action :set_user, only: [:index, :create, :destroy]

    # GET /user_roles
    def index
      authorize :user_role

      @roles = @user.roles

      respond_with @roles
    end

    # POST /user_roles
    def create
      authorize :user_role

      @role = @user.add_role(user_role_params[:name])

      respond_with @role
    end

    # DELETE /user_roles
    def destroy
      authorize :user_role

      @user.remove_role(user_role_params[:name])
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = if params.key?(:user_id)
                User.find(params[:user_id])
              else
                current_user
              end
    end

    # Only allow a trusted parameter "white list" through.
    def user_role_params
      params.require(:user_role).permit(:name)
    end
  end
end
