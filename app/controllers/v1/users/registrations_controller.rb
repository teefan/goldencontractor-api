# frozen_string_literal: true
module V1
  module Users
    class RegistrationsController < Devise::RegistrationsController
      respond_to :json

      before_action :configure_sign_up_params, only: [:create]
      before_action :configure_account_update_params, only: [:update]

      # GET /resource/sign_up
      # def new
      #   super
      # end

      # POST /resource
      def create # rubocop:disable AbcSize
        build_resource(sign_up_params)

        resource.save
        yield resource if block_given?
        if resource.persisted?
          if resource.active_for_authentication?
            sign_up(resource_name, resource)
            respond_with resource, location: after_sign_up_path_for(resource)
          else
            expire_data_after_sign_in!
            respond_with resource, location: after_inactive_sign_up_path_for(resource)
          end
        else
          clean_up_passwords resource
          set_minimum_password_length
          respond_with resource
        end
      end

      # GET /resource/edit
      # def edit
      #   super
      # end

      # PUT /resource
      def update
        super
      end

      # DELETE /resource
      def destroy
        super
      end

      # GET /resource/cancel
      # Forces the session data which is usually expired after sign
      # in to be expired now. This is useful if the user wants to
      # cancel oauth signing in/up in the middle of the process,
      # removing all OAuth session data.
      def cancel
        super
      end

      protected

      def nested_params
        {
          user_profile_attributes:
            [:id, :name, :gender, :date_of_birth, :phone_number, :street_address, :avatar, :country_id, :city_id,
             :district_id],
          company_attributes:
            [:id, :name, :tax_number, :street_address, :website_url, :country_id, :city_id, :district_id]
        }
      end

      # By default we want to require a password checks on update.
      # You can overwrite this method in your own RegistrationsController.
      def update_resource(resource, params)
        if params[:password].blank? && params[:email].blank? && params[:current_password].blank?
          resource.update_without_password(params)
        else
          resource.update_with_password(params)
        end
      end

      # If you have extra params to permit, append them to the sanitizer.
      def configure_sign_up_params
        devise_parameter_sanitizer.permit(:sign_up) do |user_params|
          user_params.permit(nested_params, :id, :email, :password, :password_confirmation, :current_password)
        end
      end

      # If you have extra params to permit, append them to the sanitizer.
      def configure_account_update_params
        devise_parameter_sanitizer.permit(:account_update) do |user_params|
          user_params.permit(nested_params, :id, :email, :password, :password_confirmation, :current_password)
        end
      end

      # The path used after sign up.
      def after_sign_up_path_for(resource)
        super(resource)
      end

      # The path used after sign up for inactive accounts.
      def after_inactive_sign_up_path_for(resource)
        super(resource)
      end
    end
  end
end
