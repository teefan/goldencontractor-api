# frozen_string_literal: true
module V1
  class UsersController < ApplicationController
    include RegistrationsPathDocument
    include SessionsPathDocument
    include PasswordsPathDocument
    include UsersPathDocument

    before_action :authenticate_user!

    # GET /users
    def index
      @users = paginate(User.includes(:user_profile, :company).filter(filtering_params))

      respond_with @users
    end

    # GET /users/1
    def show
      @user = User.find(params[:id])

      respond_with @user
    end

    # GET /me
    def me
      respond_with current_user
    end

    private

    # A list of the param names that can be used for filtering
    def filtering_params
      params.slice(:email)
    end
  end
end
