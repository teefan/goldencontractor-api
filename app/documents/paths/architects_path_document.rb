# frozen_string_literal: true
# rubocop:disable BlockLength, ModuleLength
module ArchitectsPathDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_path '/architects' do
      operation :get do
        key :summary, 'Returns all architects'
        key :operationId, 'find_architects'
        key :tags, ['architect']

        parameter do
          key :name, :email
          key :in, :query
          key :type, :string
        end

        parameter do
          key :name, :country_id
          key :in, :query
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, :city_id
          key :in, :query
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, :district_id
          key :in, :query
          key :type, :integer
          key :format, :int32
        end

        response 200 do
          key :description, 'Architects response'
          schema do
            key :type, :array
            items { key :'$ref', :Architect }
          end
        end
      end

      operation :post do
        key :summary, 'Add new architect'
        key :operationId, 'add_architect'
        key :tags, ['architect']

        parameter do
          key :name, 'architect[name]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'architect[description]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'architect[email]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'architect[phone_number]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'architect[address]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'architect[country_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'architect[city_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'architect[district_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        response 201 do
          key :description, 'Architect created'
          schema do
            key :type, :array
            items { key :'$ref', :Architect }
          end
        end
      end
    end

    swagger_path '/architects/{id}' do
      operation :get do
        key :summary, 'Returns a single architect'
        key :operationId, 'find_architect_by_id'
        key :tags, ['architect']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of architect to fetch'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        response 200 do
          key :description, 'Architect response'
          schema do
            key :'$ref', :Architect
          end
        end
      end

      operation :patch do
        key :summary, 'Update architect'
        key :operationId, 'update_architect'
        key :tags, ['architect']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of architect to update'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'architect[name]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'architect[description]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'architect[email]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'architect[phone_number]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'architect[address]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'architect[country_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'architect[city_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'architect[district_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        response 204 do
          key :description, 'Architect updated - no content'
        end
      end

      operation :delete do
        key :summary, 'Remove architect'
        key :operationId, 'remove_architect'
        key :tags, ['architect']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of architect to remove'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        response 204 do
          key :description, 'Architect removed - no content'
        end
      end
    end
  end
end
