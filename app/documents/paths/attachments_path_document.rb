# frozen_string_literal: true
# rubocop:disable BlockLength, ModuleLength
module AttachmentsPathDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_path '/attachments' do
      operation :get do
        key :summary, 'Returns all attachments'
        key :operationId, 'find_attachments'
        key :tags, ['attachment']

        parameter do
          key :name, :attachable_id
          key :in, :query
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, :attachable_type
          key :in, :query
          key :type, :string
        end

        parameter do
          key :name, :user_id
          key :in, :query
          key :type, :integer
          key :format, :int32
        end

        response 200 do
          key :description, 'Attachments response'
          schema do
            key :type, :array
            items { key :'$ref', :Attachment }
          end
        end
      end

      operation :post do
        key :summary, 'Add new attachment'
        key :operationId, 'add_attachment'
        key :tags, ['attachment']

        parameter do
          key :name, 'attachment[file]'
          key :in, :formData
          key :type, :file
        end

        parameter do
          key :name, 'attachment[files][]'
          key :description, 'Support multiple files upload in one call'
          key :in, :formData
          key :type, :array
          items do
            key :type, :file
          end
        end

        parameter do
          key :name, 'attachment[remote_file_url]'
          key :description, 'Support for remote file URL upload'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'attachment[attachable_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'attachment[attachable_type]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'attachment[user_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        response 201 do
          key :description, 'Attachment response'
          schema do
            key :type, :array
            items { key :'$ref', :Attachment }
          end
        end
      end
    end

    swagger_path '/attachments/{id}' do
      operation :get do
        key :summary, 'Returns a single attachment'
        key :operationId, 'find_attachment_by_id'
        key :tags, ['attachment']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of attachment to fetch'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        response 200 do
          key :description, 'Attachment response'
          schema do
            key :'$ref', :Attachment
          end
        end
      end

      operation :patch do
        key :summary, 'Update attachment'
        key :operationId, 'update_attachment'
        key :tags, ['attachment']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of attachment to update'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'attachment[file]'
          key :in, :formData
          key :type, :file
        end

        parameter do
          key :name, 'attachment[files][]'
          key :description, 'Support multiple files upload in one call'
          key :in, :formData
          key :type, :array
          items do
            key :type, :file
          end
        end

        parameter do
          key :name, 'attachment[remote_file_url]'
          key :description, 'Support for remote file URL upload'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'attachment[attachable_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'attachment[attachable_type]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'attachment[user_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        response 204 do
          key :description, 'Attachment updated - no content'
        end
      end

      operation :delete do
        key :summary, 'Remove attachment'
        key :operationId, 'remove_attachment'
        key :tags, ['attachment']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of attachment to remove'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        response 204 do
          key :description, 'Attachment removed - no content'
        end
      end
    end
  end
end
