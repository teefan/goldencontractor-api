# frozen_string_literal: true
# rubocop:disable BlockLength, ModuleLength
module BidsPathDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_path '/bids' do
      operation :get do
        key :summary, 'Returns all bids'
        key :operationId, 'find_bids'
        key :tags, ['bid']

        parameter do
          key :name, :job_id
          key :in, :query
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, :user_id
          key :in, :query
          key :type, :integer
          key :format, :int32
        end

        response 200 do
          key :description, 'Bids response'
          schema do
            key :type, :array
            items { key :'$ref', :Bid }
          end
        end
      end

      operation :post do
        key :summary, 'Add new bid'
        key :operationId, 'add_bid'
        key :tags, ['bid']

        parameter do
          key :name, 'bid[job_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'bid[user_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        response 201 do
          key :description, 'Bid created'
          schema do
            key :type, :array
            items { key :'$ref', :Bid }
          end
        end
      end
    end

    swagger_path '/bids/{id}' do
      operation :get do
        key :summary, 'Returns a single bid'
        key :operationId, 'find_bid_by_id'
        key :tags, ['bid']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of bid to fetch'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        response 200 do
          key :description, 'Bid response'
          schema do
            key :'$ref', :Bid
          end
        end
      end

      operation :patch do
        key :summary, 'Update bid'
        key :operationId, 'update_bid'
        key :tags, ['bid']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of bid to update'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'bid[job_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'bid[user_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'bid[flow_action]'
          key :description, 'Update bid flow'
          key :in, :formData
          key :type, :string

          items do
            key :type, :string
            key :enum, Bid::VALID_BID_FLOWS
          end
        end

        response 204 do
          key :description, 'Bid updated - no content'
        end
      end

      operation :delete do
        key :summary, 'Remove bid'
        key :operationId, 'remove_bid'
        key :tags, ['bid']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of bid to remove'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        response 204 do
          key :description, 'Bid removed - no content'
        end
      end
    end
  end
end
