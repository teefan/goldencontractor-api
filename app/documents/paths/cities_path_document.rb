# frozen_string_literal: true
# rubocop:disable BlockLength, ModuleLength
module CitiesPathDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_path '/cities' do
      operation :get do
        key :summary, 'Returns all cities'
        key :operationId, 'find_cities'
        key :tags, ['city']

        parameter do
          key :name, :country_id
          key :in, :query
          key :type, :integer
          key :format, :int32
        end

        response 200 do
          key :description, 'Cities response'
          schema do
            key :type, :array
            items { key :'$ref', :City }
          end
        end
      end

      operation :post do
        key :summary, 'Add new city'
        key :operationId, 'add_city'
        key :tags, ['city']

        parameter do
          key :name, 'city[name]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'city[active]'
          key :in, :formData
          key :type, :boolean
        end

        parameter do
          key :name, 'city[country_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        response 201 do
          key :description, 'City response'
          schema do
            key :type, :array
            items { key :'$ref', :City }
          end
        end
      end
    end

    swagger_path '/cities/{id}' do
      operation :get do
        key :summary, 'Returns a single city'
        key :operationId, 'find_city_by_id'
        key :tags, ['city']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of city to fetch'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        response 200 do
          key :description, 'City response'
          schema do
            key :'$ref', :City
          end
        end
      end

      operation :patch do
        key :summary, 'Update city'
        key :operationId, 'update_city'
        key :tags, ['city']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of city to update'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'city[name]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'city[active]'
          key :in, :formData
          key :type, :boolean
        end

        parameter do
          key :name, 'city[country_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        response 204 do
          key :description, 'City updated - no content'
        end
      end

      operation :delete do
        key :summary, 'Remove city'
        key :operationId, 'remove_city'
        key :tags, ['city']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of city to remove'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        response 204 do
          key :description, 'City removed - no content'
        end
      end
    end
  end
end
