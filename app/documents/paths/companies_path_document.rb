# frozen_string_literal: true
# rubocop:disable BlockLength, ModuleLength
module CompaniesPathDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_path '/companies' do
      operation :get do
        key :summary, 'Returns all companies'
        key :operationId, 'find_companies'
        key :tags, ['company']

        parameter do
          key :name, :tax_number
          key :in, :query
          key :type, :string
        end

        parameter do
          key :name, :country_id
          key :in, :query
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, :city_id
          key :in, :query
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, :district_id
          key :in, :query
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, :user_id
          key :in, :query
          key :type, :integer
          key :format, :int32
        end

        response 200 do
          key :description, 'Companies response'
          schema do
            key :type, :array
            items { key :'$ref', :Company }
          end
        end
      end

      operation :post do
        key :summary, 'Add new company'
        key :operationId, 'add_company'
        key :tags, ['company']

        parameter do
          key :name, 'company[name]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'company[tax_number]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'company[street_address]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'company[website_url]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'company[avatar]'
          key :in, :formData
          key :type, :file
        end

        parameter do
          key :name, 'company[country_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'company[city_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'company[district_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'company[user_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        response 201 do
          key :description, 'Company created'
          schema do
            key :type, :array
            items { key :'$ref', :Company }
          end
        end
      end
    end

    swagger_path '/companies/{id}' do
      operation :get do
        key :summary, 'Returns a single company'
        key :operationId, 'find_company_by_id'
        key :tags, ['company']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of company to fetch'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        response 200 do
          key :description, 'Company response'
          schema do
            key :'$ref', :Company
          end
        end
      end

      operation :patch do
        key :summary, 'Update company'
        key :operationId, 'update_company'
        key :tags, ['company']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of company to update'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'company[name]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'company[tax_number]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'company[street_address]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'company[website_url]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'company[avatar]'
          key :in, :formData
          key :type, :file
        end

        parameter do
          key :name, 'company[country_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'company[city_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'company[district_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'company[user_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        response 204 do
          key :description, 'Company updated - no content'
        end
      end

      operation :delete do
        key :summary, 'Remove company'
        key :operationId, 'remove_company'
        key :tags, ['company']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of company to remove'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        response 204 do
          key :description, 'Company removed - no content'
        end
      end
    end
  end
end
