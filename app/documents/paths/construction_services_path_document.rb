# frozen_string_literal: true
# rubocop:disable BlockLength, ModuleLength
module ConstructionServicesPathDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_path '/construction_services' do
      operation :get do
        key :summary, 'Returns all construction services'
        key :operationId, 'find_construction_services'
        key :tags, ['construction_service']

        response 200 do
          key :description, 'Construction services response'
          schema do
            key :type, :array
            items { key :'$ref', :ConstructionService }
          end
        end
      end

      operation :post do
        key :summary, 'Add new construction_service'
        key :operationId, 'add_construction_service'
        key :tags, ['construction_service']

        parameter do
          key :name, 'construction_service[name]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'construction_service[active]'
          key :in, :formData
          key :type, :boolean
        end

        response 201 do
          key :description, 'Construction service created'
          schema do
            key :type, :array
            items { key :'$ref', :ConstructionService }
          end
        end
      end
    end

    swagger_path '/construction_services/{id}' do
      operation :get do
        key :summary, 'Returns a single construction service'
        key :operationId, 'find_construction_service_by_id'
        key :tags, ['construction_service']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of construction service to fetch'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        response 200 do
          key :description, 'Construction service response'
          schema do
            key :'$ref', :ConstructionService
          end
        end
      end

      operation :patch do
        key :summary, 'Update construction service'
        key :operationId, 'update_construction_service'
        key :tags, ['construction_service']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of construction service to update'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'construction_service[name]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'construction_service[active]'
          key :in, :formData
          key :type, :boolean
        end

        response 204 do
          key :description, 'Construction service updated - no content'
        end
      end

      operation :delete do
        key :summary, 'Remove construction service'
        key :operationId, 'remove_construction_service'
        key :tags, ['construction_service']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of construction service to remove'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        response 204 do
          key :description, 'Construction service removed - no content'
        end
      end
    end
  end
end
