# frozen_string_literal: true
# rubocop:disable BlockLength, ModuleLength
module ConstructionTypesPathDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_path '/construction_types' do
      operation :get do
        key :summary, 'Returns all construction types'
        key :operationId, 'find_construction_types'
        key :tags, ['construction_type']

        response 200 do
          key :description, 'Construction types response'
          schema do
            key :type, :array
            items { key :'$ref', :ConstructionType }
          end
        end
      end

      operation :post do
        key :summary, 'Add new construction_type'
        key :operationId, 'add_construction_type'
        key :tags, ['construction_type']

        parameter do
          key :name, 'construction_type[name]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'construction_type[active]'
          key :in, :formData
          key :type, :boolean
        end

        response 201 do
          key :description, 'Construction type created'
          schema do
            key :type, :array
            items { key :'$ref', :ConstructionType }
          end
        end
      end
    end

    swagger_path '/construction_types/{id}' do
      operation :get do
        key :summary, 'Returns a single construction type'
        key :operationId, 'find_construction_type_by_id'
        key :tags, ['construction_type']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of construction type to fetch'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        response 200 do
          key :description, 'Construction type response'
          schema do
            key :'$ref', :ConstructionType
          end
        end
      end

      operation :patch do
        key :summary, 'Update construction type'
        key :operationId, 'update_construction_type'
        key :tags, ['construction_type']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of construction type to update'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'construction_type[name]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'construction_type[active]'
          key :in, :formData
          key :type, :boolean
        end

        response 204 do
          key :description, 'Construction type updated - no content'
        end
      end

      operation :delete do
        key :summary, 'Remove construction type'
        key :operationId, 'remove_construction_type'
        key :tags, ['construction_type']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of construction type to remove'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        response 204 do
          key :description, 'Construction type removed - no content'
        end
      end
    end
  end
end
