# frozen_string_literal: true
# rubocop:disable BlockLength, ModuleLength
module CountriesPathDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_path '/countries' do
      operation :get do
        key :summary, 'Returns all countries'
        key :operationId, 'find_countries'
        key :tags, ['country']

        parameter do
          key :name, :alpha2
          key :in, :query
          key :type, :string
        end

        response 200 do
          key :description, 'Countries response'
          schema do
            key :type, :array
            items { key :'$ref', :Country }
          end
        end
      end

      operation :post do
        key :summary, 'Add new country'
        key :operationId, 'add_country'
        key :tags, ['country']

        parameter do
          key :name, 'country[name]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'country[alpha2]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'country[active]'
          key :in, :formData
          key :type, :boolean
        end

        response 201 do
          key :description, 'Country created'
          schema do
            key :type, :array
            items { key :'$ref', :Country }
          end
        end
      end
    end

    swagger_path '/countries/{id}' do
      operation :get do
        key :summary, 'Returns a single country'
        key :operationId, 'find_country_by_id'
        key :tags, ['country']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of country to fetch'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        response 200 do
          key :description, 'Country response'
          schema do
            key :'$ref', :Country
          end
        end
      end

      operation :patch do
        key :summary, 'Update country'
        key :operationId, 'update_country'
        key :tags, ['country']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of country to update'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'country[name]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'country[alpha2]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'country[active]'
          key :in, :formData
          key :type, :boolean
        end

        response 204 do
          key :description, 'Country updated - no content'
        end
      end

      operation :delete do
        key :summary, 'Remove country'
        key :operationId, 'remove_country'
        key :tags, ['country']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of country to remove'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        response 204 do
          key :description, 'Country removed - no content'
        end
      end
    end
  end
end
