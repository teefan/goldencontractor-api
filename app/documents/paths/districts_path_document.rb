# frozen_string_literal: true
# rubocop:disable BlockLength, ModuleLength
module DistrictsPathDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_path '/districts' do
      operation :get do
        key :summary, 'Returns all districts'
        key :operationId, 'find_districts'
        key :tags, ['district']

        parameter do
          key :name, :country_id
          key :in, :query
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, :city_id
          key :in, :query
          key :type, :integer
          key :format, :int32
        end

        response 200 do
          key :description, 'Districts response'
          schema do
            key :type, :array
            items { key :'$ref', :District }
          end
        end
      end

      operation :post do
        key :summary, 'Add new district'
        key :operationId, 'add_district'
        key :tags, ['district']

        parameter do
          key :name, 'district[name]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'district[active]'
          key :in, :formData
          key :type, :boolean
        end

        parameter do
          key :name, 'district[country_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'district[city_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        response 201 do
          key :description, 'District response'
          schema do
            key :type, :array
            items { key :'$ref', :District }
          end
        end
      end
    end

    swagger_path '/districts/{id}' do
      operation :get do
        key :summary, 'Returns a single district'
        key :operationId, 'find_district_by_id'
        key :tags, ['district']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of district to fetch'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        response 200 do
          key :description, 'District response'
          schema do
            key :'$ref', :District
          end
        end
      end

      operation :patch do
        key :summary, 'Update district'
        key :operationId, 'update_district'
        key :tags, ['district']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of district to update'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'district[name]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'district[active]'
          key :in, :formData
          key :type, :boolean
        end

        parameter do
          key :name, 'district[country_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'district[city_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        response 204 do
          key :description, 'District updated - no content'
        end
      end

      operation :delete do
        key :summary, 'Remove district'
        key :operationId, 'remove_district'
        key :tags, ['district']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of district to remove'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        response 204 do
          key :description, 'District removed - no content'
        end
      end
    end
  end
end
