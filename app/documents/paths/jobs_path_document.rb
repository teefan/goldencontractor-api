# frozen_string_literal: true
# rubocop:disable BlockLength, ModuleLength
module JobsPathDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_path '/jobs' do
      operation :get do
        key :summary, 'Returns all jobs'
        key :operationId, 'find_jobs'
        key :tags, ['job']

        parameter do
          key :name, :construction_service_id
          key :in, :query
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, :construction_type_id
          key :in, :query
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, :country_id
          key :in, :query
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, :city_id
          key :in, :query
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, :district_id
          key :in, :query
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, :user_id
          key :in, :query
          key :type, :integer
          key :format, :int32
        end

        response 200 do
          key :description, 'Jobs response'
          schema do
            key :type, :array
            items { key :'$ref', :Job }
          end
        end
      end

      operation :post do
        key :summary, 'Add new job'
        key :operationId, 'add_job'
        key :tags, ['job']

        parameter do
          key :name, 'job[title]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'job[description]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'job[land_width]'
          key :in, :formData
          key :type, :number
          key :format, :float
        end

        parameter do
          key :name, 'job[land_length]'
          key :in, :formData
          key :type, :number
          key :format, :float
        end

        parameter do
          key :name, 'job[land_area]'
          key :in, :formData
          key :type, :number
          key :format, :float
        end

        parameter do
          key :name, 'job[floor_width]'
          key :in, :formData
          key :type, :number
          key :format, :float
        end

        parameter do
          key :name, 'job[floor_length]'
          key :in, :formData
          key :type, :number
          key :format, :float
        end

        parameter do
          key :name, 'job[floor_area]'
          key :in, :formData
          key :type, :number
          key :format, :float
        end

        parameter do
          key :name, 'job[number_of_floors]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'job[has_terrace]'
          key :in, :formData
          key :type, :boolean
        end

        parameter do
          key :name, 'job[has_mezzanine]'
          key :in, :formData
          key :type, :boolean
        end

        parameter do
          key :name, 'job[has_basement]'
          key :in, :formData
          key :type, :boolean
        end

        parameter do
          key :name, 'job[planned_start_date]'
          key :in, :formData
          key :type, :string
          key :format, :date
        end

        parameter do
          key :name, 'job[estimated_cost]'
          key :in, :formData
          key :type, :number
          key :format, :double
        end

        parameter do
          key :name, 'job[budget_level]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'job[budget]'
          key :in, :formData
          key :type, :number
          key :format, :double
        end

        parameter do
          key :name, 'job[verified]'
          key :in, :formData
          key :type, :boolean
        end

        parameter do
          key :name, 'job[published_at]'
          key :in, :formData
          key :type, :string
          key :format, :date
        end

        parameter do
          key :name, 'job[next_processing_date]'
          key :in, :formData
          key :type, :string
          key :format, :date
        end

        parameter do
          key :name, 'job[max_contractors]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'job[address]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'job[construction_service_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'job[construction_type_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'job[country_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'job[city_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'job[district_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'job[user_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'job[attachment_ids][]'
          key :description, 'Support attach uploaded files into job. Attached files will be ignored.'
          key :in, :formData
          key :type, :array
          items do
            key :type, :integer
            key :format, :int32
          end
        end

        response 201 do
          key :description, 'Job response'
          schema do
            key :type, :array
            items { key :'$ref', :Job }
          end
        end
      end
    end

    swagger_path '/jobs/{id}' do
      operation :get do
        key :summary, 'Returns a single job'
        key :operationId, 'find_job_by_id'
        key :tags, ['job']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of job to fetch'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        response 200 do
          key :description, 'Job response'
          schema do
            key :'$ref', :Job
          end
        end
      end

      operation :patch do
        key :summary, 'Update job'
        key :operationId, 'update_job'
        key :tags, ['job']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of job to update'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'job[title]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'job[description]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'job[land_width]'
          key :in, :formData
          key :type, :number
          key :format, :float
        end

        parameter do
          key :name, 'job[land_length]'
          key :in, :formData
          key :type, :number
          key :format, :float
        end

        parameter do
          key :name, 'job[land_area]'
          key :in, :formData
          key :type, :number
          key :format, :float
        end

        parameter do
          key :name, 'job[floor_width]'
          key :in, :formData
          key :type, :number
          key :format, :float
        end

        parameter do
          key :name, 'job[floor_length]'
          key :in, :formData
          key :type, :number
          key :format, :float
        end

        parameter do
          key :name, 'job[floor_area]'
          key :in, :formData
          key :type, :number
          key :format, :float
        end

        parameter do
          key :name, 'job[number_of_floors]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'job[has_terrace]'
          key :in, :formData
          key :type, :boolean
        end

        parameter do
          key :name, 'job[has_mezzanine]'
          key :in, :formData
          key :type, :boolean
        end

        parameter do
          key :name, 'job[has_basement]'
          key :in, :formData
          key :type, :boolean
        end

        parameter do
          key :name, 'job[planned_start_date]'
          key :in, :formData
          key :type, :string
          key :format, :date
        end

        parameter do
          key :name, 'job[estimated_cost]'
          key :in, :formData
          key :type, :number
          key :format, :double
        end

        parameter do
          key :name, 'job[budget_level]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'job[budget]'
          key :in, :formData
          key :type, :number
          key :format, :double
        end

        parameter do
          key :name, 'job[verified]'
          key :in, :formData
          key :type, :boolean
        end

        parameter do
          key :name, 'job[published_at]'
          key :in, :formData
          key :type, :string
          key :format, :date
        end

        parameter do
          key :name, 'job[next_processing_date]'
          key :in, :formData
          key :type, :string
          key :format, :date
        end

        parameter do
          key :name, 'job[max_contractors]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'job[address]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'job[flow_action]'
          key :description, 'Update job workflow'
          key :in, :formData
          key :type, :string

          items do
            key :type, :string
            key :enum, Job::VALID_WORKFLOWS
          end
        end

        parameter do
          key :name, 'job[construction_service_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'job[construction_type_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'job[country_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'job[city_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'job[district_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'job[user_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'job[attachment_ids][]'
          key :description, 'Support attach uploaded files into job. Attached files will be ignored.'
          key :in, :formData
          key :type, :array
          items do
            key :type, :integer
            key :format, :int32
          end
        end

        response 204 do
          key :description, 'Job updated - no content'
        end
      end

      operation :delete do
        key :summary, 'Remove job'
        key :operationId, 'remove_job'
        key :tags, ['job']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of job to remove'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        response 204 do
          key :description, 'Job removed - no content'
        end
      end
    end
  end
end
