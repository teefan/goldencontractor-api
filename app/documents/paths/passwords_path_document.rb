# frozen_string_literal: true
# rubocop:disable BlockLength
module PasswordsPathDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_path '/users/password' do
      operation :post do
        key :summary, 'User request password reset'
        key :description, 'User will receive password reset instructions email'
        key :operationId, 'request_password_reset'
        key :tags, ['password']

        parameter do
          key :name, 'user[email]'
          key :in, :formData
          key :type, :string
        end

        response 201 do
          key :description, 'Password reset email created'
        end
      end

      operation :patch do
        key :summary, 'User password update'
        key :operationId, 'update_password'
        key :tags, ['password']

        parameter do
          key :name, 'user[reset_password_token]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'user[password]'
          key :in, :formData
          key :type, :string
          key :format, :password
        end

        parameter do
          key :name, 'user[password_confirmation]'
          key :in, :formData
          key :type, :string
          key :format, :password
        end

        response 204 do
          key :description, 'User password updated - no response'
        end
      end
    end
  end
end
