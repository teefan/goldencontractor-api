# frozen_string_literal: true
# rubocop:disable BlockLength, ModuleLength
module RegistrationsPathDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_path '/users' do
      operation :post do
        key :summary, 'User registration'
        key :operationId, 'sign_up_user'
        key :tags, ['account']

        parameter do
          key :name, 'user[email]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'user[password]'
          key :in, :formData
          key :type, :string
          key :format, :password
        end

        parameter do
          key :name, 'user[password_confirmation]'
          key :description, '(Optional) password confirmation'
          key :in, :formData
          key :type, :string
          key :format, :password
        end

        parameter do
          key :name, 'user[user_profile_attributes[name]]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'user[user_profile_attributes[gender]]'
          key :in, :formData
          key :type, :string
          items do
            key :type, :string
            key :enum, %w(male female)
          end
        end

        parameter do
          key :name, 'user[user_profile_attributes[date_of_birth]]'
          key :in, :formData
          key :type, :string
          key :format, :date
        end

        parameter do
          key :name, 'user[user_profile_attributes[phone_number]]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'user[user_profile_attributes[street_address]]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'user[user_profile_attributes[contact_time]]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'user[user_profile_attributes[avatar]]'
          key :in, :formData
          key :type, :file
        end

        parameter do
          key :name, 'user[user_profile_attributes[country_id]]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'user[user_profile_attributes[city_id]]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'user[user_profile_attributes[district_id]]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'user[company_attributes[name]]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'user[company_attributes[tax_number]]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'user[company_attributes[country_id]]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'user[company_attributes[city_id]]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'user[company_attributes[district_id]]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        response 200 do
          key :description, 'Registration response'
          schema do
            key :type, :array
            items { key :'$ref', :AuthenticatedUser }
          end
        end
      end

      operation :patch do
        key :summary, 'User account update'
        key :operationId, 'update_user'
        key :tags, ['account']

        parameter do
          key :name, 'user[email]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'user[password]'
          key :in, :formData
          key :type, :string
          key :format, :password
        end

        parameter do
          key :name, 'user[password_confirmation]'
          key :description, '(Optional) password confirmation'
          key :in, :formData
          key :type, :string
          key :format, :password
        end

        parameter do
          key :name, 'user[current_password]'
          key :description, '(Optional) required current password when updating account'
          key :in, :formData
          key :type, :string
          key :format, :password
        end

        parameter do
          key :name, 'user[user_profile_attributes[id]]'
          key :description, 'Required when updating account to not DELETE the profile'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'user[user_profile_attributes[name]]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'user[user_profile_attributes[gender]]'
          key :in, :formData
          key :type, :string
          items do
            key :type, :string
            key :enum, %w(male female)
          end
        end

        parameter do
          key :name, 'user[user_profile_attributes[date_of_birth]]'
          key :in, :formData
          key :type, :string
          key :format, :date
        end

        parameter do
          key :name, 'user[user_profile_attributes[phone_number]]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'user[user_profile_attributes[street_address]]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'user[user_profile_attributes[contact_time]]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'user[user_profile_attributes[verified_contractor]]'
          key :in, :formData
          key :type, :boolean
        end

        parameter do
          key :name, 'user[user_profile_attributes[avatar]]'
          key :in, :formData
          key :type, :file
        end

        parameter do
          key :name, 'user[user_profile_attributes[country_id]]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'user[user_profile_attributes[city_id]]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'user[user_profile_attributes[district_id]]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'user[company_attributes[name]]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'user[company_attributes[tax_number]]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'user[company_attributes[country_id]]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'user[company_attributes[city_id]]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'user[company_attributes[district_id]]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        response 204 do
          key :description, 'User account updated - no content'
        end
      end

      operation :delete do
        key :summary, 'User account delete'
        key :operationId, 'delete_user'
        key :tags, ['account']

        response 204 do
          key :description, 'User account deleted - no content'
        end
      end
    end
  end
end
