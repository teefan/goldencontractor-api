# frozen_string_literal: true
# rubocop:disable BlockLength, ModuleLength
module RolesPathDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_path '/roles' do
      operation :get do
        key :summary, 'Returns all roles'
        key :operationId, 'find_roles'
        key :tags, ['role']

        parameter do
          key :name, :resource_type
          key :in, :query
          key :type, :string
        end

        parameter do
          key :name, :resource_id
          key :in, :query
          key :type, :integer
          key :format, :int32
        end

        response 200 do
          key :description, 'Roles response'
          schema do
            key :type, :array
            items { key :'$ref', :Role }
          end
        end
      end

      operation :post do
        key :summary, 'Add new role'
        key :operationId, 'add_role'
        key :tags, ['role']

        parameter do
          key :name, 'role[name]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'role[resource_type]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'role[resource_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        response 201 do
          key :description, 'Role response'
          schema do
            key :type, :array
            items { key :'$ref', :Role }
          end
        end
      end
    end

    swagger_path '/roles/{id}' do
      operation :patch do
        key :summary, 'Update role'
        key :operationId, 'update_role'
        key :tags, ['role']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of role to update'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'role[name]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'role[resource_type]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'role[resource_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        response 204 do
          key :description, 'Role updated - no content'
        end
      end

      operation :delete do
        key :summary, 'Remove role'
        key :operationId, 'remove_role'
        key :tags, ['role']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of role to remove'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        response 204 do
          key :description, 'Role removed - no content'
        end
      end
    end
  end
end
