# frozen_string_literal: true
# rubocop:disable BlockLength
module SessionsPathDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_path '/users/sign_in' do
      operation :post do
        key :summary, 'User sign in'
        key :operationId, 'sign_in_user'
        key :tags, ['session']

        parameter do
          key :name, 'user[email]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'user[password]'
          key :in, :formData
          key :type, :string
          key :format, :password
        end

        response 201 do
          key :description, 'Session created - user signed in'
          schema do
            key :type, :array
            items { key :'$ref', :AuthenticatedUser }
          end
        end
      end
    end

    swagger_path '/users/sign_out' do
      operation :delete do
        key :summary, 'User sign out'
        key :operationId, 'sign_out_user'
        key :tags, ['session']

        response 204 do
          key :description, 'Session removed - user signed out'
        end
      end
    end
  end
end
