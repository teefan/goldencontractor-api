# frozen_string_literal: true
# rubocop:disable BlockLength, ModuleLength
module UserProfilePathDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_path '/user_profile' do
      operation :get do
        key :summary, 'Returns current user profile'
        key :operationId, 'find_current_user_profile'
        key :tags, ['user_profile']

        response 200 do
          key :description, 'User profile response'
          schema do
            key :type, :array
            items { key :'$ref', :UserProfile }
          end
        end
      end

      operation :post do
        key :summary, 'Add user profile'
        key :operationId, 'add_current_user_profile'
        key :tags, ['user_profile']

        parameter do
          key :name, 'user_profile[name]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'user_profile[gender]'
          key :in, :formData
          key :type, :string
          items do
            key :type, :string
            key :enum, %w(male female)
          end
        end

        parameter do
          key :name, 'user_profile[date_of_birth]'
          key :in, :formData
          key :type, :string
          key :format, :date
        end

        parameter do
          key :name, 'user_profile[phone_number]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'user_profile[street_address]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'user_profile[contact_time]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'user_profile[avatar]'
          key :in, :formData
          key :type, :file
        end

        parameter do
          key :name, 'user_profile[country_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'user_profile[city_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'user_profile[district_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        response 201 do
          key :description, 'User profile added'
          schema do
            key :type, :array
            items { key :'$ref', :UserProfile }
          end
        end
      end

      operation :patch do
        key :summary, 'Update user profile'
        key :operationId, 'update_user_profile'
        key :tags, ['user_profile']

        parameter do
          key :name, 'user_profile[name]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'user_profile[gender]'
          key :in, :formData
          key :type, :string
          items do
            key :type, :string
            key :enum, %w(male female)
          end
        end

        parameter do
          key :name, 'user_profile[date_of_birth]'
          key :in, :formData
          key :type, :string
          key :format, :date
        end

        parameter do
          key :name, 'user_profile[phone_number]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'user_profile[street_address]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'user_profile[contact_time]'
          key :in, :formData
          key :type, :string
        end

        parameter do
          key :name, 'user_profile[verified_contractor]'
          key :in, :formData
          key :type, :boolean
        end

        parameter do
          key :name, 'user_profile[avatar]'
          key :in, :formData
          key :type, :file
        end

        parameter do
          key :name, 'user_profile[country_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'user_profile[city_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        parameter do
          key :name, 'user_profile[district_id]'
          key :in, :formData
          key :type, :integer
          key :format, :int32
        end

        response 201 do
          key :description, 'User profile added'
          schema do
            key :type, :array
            items { key :'$ref', :UserProfile }
          end
        end
      end

      operation :delete do
        key :summary, 'Remove current user profile'
        key :operationId, 'remove_current_user_profile'
        key :tags, ['user_profile']

        response 204 do
          key :description, 'User profile removed - no content'
        end
      end
    end

    swagger_path '/user_profiles/{id}' do
      operation :get do
        key :summary, 'Returns a user profile'
        key :operationId, 'find_user_profile_by_id'
        key :tags, ['user_profile']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of user profile to fetch'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        response 200 do
          key :description, 'User profile response'
          schema do
            key :'$ref', :UserProfile
          end
        end
      end
    end
  end
end
