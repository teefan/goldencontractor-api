# frozen_string_literal: true
# rubocop:disable BlockLength
module UserRolesPathDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_path '/user_roles' do
      operation :get do
        key :summary, 'Returns all roles of a user'
        key :operationId, 'list_user_roles'
        key :tags, ['user_role']

        parameter do
          key :name, 'user_id'
          key :description, '(Optional) Get current user if user_id is not present'
          key :in, :query
          key :type, :string
        end

        response 200 do
          key :description, 'User role response'
          schema do
            key :type, :array
            items { key :'$ref', :Role }
          end
        end
      end

      operation :post do
        key :summary, 'Add new user role'
        key :operationId, 'add_user_role'
        key :tags, ['user_role']

        parameter do
          key :name, 'user_id'
          key :description, '(Optional) Get current user if user_id is not present'
          key :in, :query
          key :type, :string
        end

        parameter do
          key :name, 'user_role[name]'
          key :in, :formData
          key :type, :string
        end

        response 201 do
          key :description, 'Role added'
          schema do
            key :type, :array
            items { key :'$ref', :Role }
          end
        end
      end

      operation :delete do
        key :summary, 'Remove user role'
        key :operationId, 'remove_user_role'
        key :tags, ['user_role']

        parameter do
          key :name, 'user_id'
          key :description, '(Optional) Get current user if user_id is not present'
          key :in, :query
          key :type, :string
        end

        parameter do
          key :name, 'user_role[name]'
          key :in, :formData
          key :type, :string
        end

        response 204 do
          key :description, 'Role removed - no content'
        end
      end
    end
  end
end
