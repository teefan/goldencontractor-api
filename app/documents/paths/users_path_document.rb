# frozen_string_literal: true
# rubocop:disable BlockLength
module UsersPathDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_path '/me' do
      operation :get do
        key :summary, 'Returns current user'
        key :operationId, 'get_current_user'
        key :tags, ['user']

        response 200 do
          key :description, 'Current user response'
          schema do
            key :'$ref', :AuthenticatedUser
          end
        end
      end
    end

    swagger_path '/users' do
      operation :get do
        key :summary, 'Returns all users from the system'
        key :operationId, 'find_users'
        key :tags, ['user']

        parameter do
          key :name, :email
          key :in, :query
          key :type, :string
        end

        response 200 do
          key :description, 'Users response'
          schema do
            key :type, :array
            items { key :'$ref', :User }
          end
        end
      end
    end

    swagger_path '/users/{id}' do
      operation :get do
        key :summary, 'Returns a single user'
        key :operationId, 'find_user_by_id'
        key :tags, ['user']

        parameter do
          key :name, :id
          key :in, :path
          key :description, 'ID of user to fetch'
          key :required, true
          key :type, :integer
          key :format, :int32
        end

        response 200 do
          key :description, 'User response'
          schema do
            key :'$ref', :User
          end
        end
      end
    end
  end
end
