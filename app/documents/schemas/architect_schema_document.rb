# frozen_string_literal: true
# rubocop:disable BlockLength
module ArchitectSchemaDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_schema :Architect do
      key :title, :Architect
      key :required, [:id, :name, :description, :email, :phone_number, :address, :country_id, :city_id, :district_id]

      property :id do
        key :type, :integer
        key :format, :int32
      end

      property :name do
        key :type, :string
      end

      property :description do
        key :type, :string
      end

      property :email do
        key :type, :string
      end

      property :phone_number do
        key :type, :string
      end

      property :address do
        key :type, :string
      end

      property :country_id do
        key :type, :integer
        key :format, :int32
      end

      property :city_id do
        key :type, :integer
        key :format, :int32
      end

      property :district_id do
        key :type, :integer
        key :format, :int32
      end
    end

    swagger_schema :ArchitectInput do
      allOf do
        schema do
          key :required, [:architect]

          property :architect do
            key :title, :architect
            key :type, :object
            key :required, [:name]

            property :description do
              key :type, :string
            end

            property :email do
              key :type, :string
            end

            property :phone_number do
              key :type, :string
            end

            property :address do
              key :type, :string
            end

            property :country_id do
              key :type, :integer
              key :format, :int32
            end

            property :city_id do
              key :type, :integer
              key :format, :int32
            end

            property :district_id do
              key :type, :integer
              key :format, :int32
            end
          end
        end
      end
    end
  end
end
