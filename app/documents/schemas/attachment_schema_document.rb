# frozen_string_literal: true
# rubocop:disable BlockLength
module AttachmentSchemaDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_schema :Attachment do
      key :title, :Attachment
      key :required, [:id, :file, :files, :file_size, :content_type, :attachable_id, :attachable_type, :user_id]

      property :id do
        key :type, :integer
        key :format, :int32
      end

      property :file do
        key :type, :file
      end

      property :file_size do
        key :type, :float
      end

      property :content_type do
        key :type, :string
      end

      property :attachable_id do
        key :type, :integer
        key :format, :int32
      end

      property :attachable_type do
        key :type, :string
      end

      property :user_id do
        key :type, :integer
        key :format, :int32
      end
    end

    swagger_schema :AttachmentInput do
      allOf do
        schema do
          key :required, [:attachment]

          property :attachment do
            key :title, :attachment
            key :type, :object
            key :required, [:file]

            property :file do
              key :type, :file
            end

            property :remote_file_url do
              key :type, :string
            end

            property :attachable_id do
              key :type, :integer
              key :format, :int32
            end

            property :attachable_type do
              key :type, :string
            end

            property :user_id do
              key :type, :integer
              key :format, :int32
            end
          end
        end
      end
    end
  end
end
