# frozen_string_literal: true
module AuthenticatedUserSchemaDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_schema :AuthenticatedUser do
      key :title, :AuthenticatedUser
      key :required, [:id, :email, :auth_token]

      property :id do
        key :type, :integer
        key :format, :int32
      end

      property :email do
        key :type, :string
      end

      property :auth_token do
        key :type, :string
        key :description, 'API resources access authentication code'
      end

      property :user_profile do
        key :type, :object
        key '$ref', :UserProfile
      end

      property :company do
        key :type, :object
        key '$ref', :Company
      end
    end
  end
end
