# frozen_string_literal: true
# rubocop:disable BlockLength
module BidSchemaDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_schema :Bid do
      key :title, :Bid
      key :required, [:id, :job_id, :user_id, :status]

      property :id do
        key :type, :integer
        key :format, :int32
      end

      property :job_id do
        key :type, :integer
        key :format, :int32
      end

      property :user_id do
        key :type, :integer
        key :format, :int32
      end

      property :status do
        key :type, :string
      end
    end

    swagger_schema :BidInput do
      allOf do
        schema do
          key :required, [:bid]

          property :bid do
            key :title, :bid
            key :type, :object
            key :required, [:job_id, :user_id, :status]

            property :job_id do
              key :type, :integer
              key :format, :int32
            end

            property :user_id do
              key :type, :integer
              key :format, :int32
            end

            property :flow_action do
              key :type, :string
            end
          end
        end
      end
    end
  end
end
