# frozen_string_literal: true
# rubocop:disable BlockLength
module CitySchemaDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_schema :City do
      key :title, :City
      key :required, [:id, :name, :active]

      property :id do
        key :type, :integer
        key :format, :int32
      end

      property :name do
        key :type, :string
      end

      property :active do
        key :type, :boolean
      end

      # property :country do
      #   key :type, :object
      #   key '$ref', :Country
      # end
    end

    swagger_schema :CityInput do
      allOf do
        schema do
          key :required, [:city]

          property :city do
            key :title, :city
            key :type, :object
            key :required, [:name, :country_id]

            property :name do
              key :type, :string
            end

            property :active do
              key :type, :boolean
            end

            property :country_id do
              key :type, :integer
              key :format, :int32
            end
          end
        end
      end
    end
  end
end
