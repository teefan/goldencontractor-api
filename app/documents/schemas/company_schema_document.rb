# frozen_string_literal: true
# rubocop:disable BlockLength, ModuleLength
module CompanySchemaDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_schema :Company do
      key :title, :Company
      key :required, [:id, :name, :tax_number, :street_address, :website_url, :avatar,
                      :country_id, :city_id, :district_id, :user_id]

      property :id do
        key :type, :integer
        key :format, :int32
      end

      property :name do
        key :type, :string
      end

      property :tax_number do
        key :type, :string
      end

      property :street_address do
        key :type, :string
      end

      property :website_url do
        key :type, :string
      end

      property :avatar do
        key :type, :file
      end

      property :country_id do
        key :type, :integer
        key :format, :int32
      end

      property :city_id do
        key :type, :integer
        key :format, :int32
      end

      property :district_id do
        key :type, :integer
        key :format, :int32
      end

      property :user_id do
        key :type, :integer
        key :format, :int32
      end
    end

    swagger_schema :CompanyInput do
      allOf do
        schema do
          key :required, [:company]

          property :company do
            key :title, :company
            key :type, :object
            key :required, [:name, :tax_number]

            property :name do
              key :type, :string
            end

            property :tax_number do
              key :type, :string
            end

            property :street_address do
              key :type, :string
            end

            property :website_url do
              key :type, :string
            end

            property :avatar do
              key :type, :file
            end

            property :country_id do
              key :type, :integer
              key :format, :int32
            end

            property :city_id do
              key :type, :integer
              key :format, :int32
            end

            property :district_id do
              key :type, :integer
              key :format, :int32
            end

            property :user_id do
              key :type, :integer
              key :format, :int32
            end
          end
        end
      end
    end

    swagger_schema :CompanyAttributesInput do
      allOf do
        schema do
          key :required, [:name, :tax_number]

          property :name do
            key :type, :string
          end

          property :tax_number do
            key :type, :string
          end

          property :street_address do
            key :type, :string
          end

          property :website_url do
            key :type, :string
          end

          property :avatar do
            key :type, :file
          end

          property :country_id do
            key :type, :integer
            key :format, :int32
          end

          property :city_id do
            key :type, :integer
            key :format, :int32
          end

          property :district_id do
            key :type, :integer
            key :format, :int32
          end

          property :user_id do
            key :type, :integer
            key :format, :int32
          end
        end
      end
    end
  end
end
