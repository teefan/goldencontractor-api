# frozen_string_literal: true
# rubocop:disable BlockLength
module ConstructionServiceSchemaDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_schema :ConstructionService do
      key :title, :ConstructionService
      key :required, [:id, :name, :active]

      property :id do
        key :type, :integer
        key :format, :int32
      end

      property :name do
        key :type, :string
      end

      property :active do
        key :type, :boolean
      end
    end

    swagger_schema :ConstructionServiceInput do
      allOf do
        schema do
          key :required, [:construction_service]

          property :construction_service do
            key :title, :construction_service
            key :type, :object
            key :required, [:name]

            property :name do
              key :type, :string
            end

            property :active do
              key :type, :boolean
            end
          end
        end
      end
    end
  end
end
