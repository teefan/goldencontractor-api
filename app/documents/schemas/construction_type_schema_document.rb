# frozen_string_literal: true
# rubocop:disable BlockLength
module ConstructionTypeSchemaDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_schema :ConstructionType do
      key :title, :ConstructionType
      key :required, [:id, :name, :active]

      property :id do
        key :type, :integer
        key :format, :int32
      end

      property :name do
        key :type, :string
      end

      property :active do
        key :type, :boolean
      end
    end

    swagger_schema :ConstructionTypeInput do
      allOf do
        schema do
          key :required, [:construction_type]

          property :construction_type do
            key :title, :construction_type
            key :type, :object
            key :required, [:name]

            property :name do
              key :type, :string
            end

            property :active do
              key :type, :boolean
            end
          end
        end
      end
    end
  end
end
