# frozen_string_literal: true
# rubocop:disable BlockLength
module CountrySchemaDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_schema :Country do
      key :title, :Country
      key :required, [:id, :name, :alpha2, :active]

      property :id do
        key :type, :integer
        key :format, :int32
      end

      property :name do
        key :type, :string
      end

      property :alpha2 do
        key :type, :string
        key :description, 'Country ISO ALPHA-2 code'
      end

      property :active do
        key :type, :boolean
      end
    end

    swagger_schema :CountryInput do
      allOf do
        schema do
          key :required, [:country]

          property :country do
            key :title, :country
            key :type, :object
            key :required, [:name]

            property :name do
              key :type, :string
            end

            property :alpha2 do
              key :type, :string
            end

            property :active do
              key :type, :boolean
            end
          end
        end
      end
    end
  end
end
