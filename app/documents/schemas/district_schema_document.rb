# frozen_string_literal: true
# rubocop:disable BlockLength
module DistrictSchemaDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_schema :District do
      key :title, :District
      key :required, [:id, :name, :active]

      property :id do
        key :type, :integer
        key :format, :int32
      end

      property :name do
        key :type, :string
      end

      property :active do
        key :type, :boolean
      end

      # property :city do
      #   key :type, :object
      #   key '$ref', :City
      # end

      # property :country do
      #   key :type, :object
      #   key '$ref', :Country
      # end
    end

    swagger_schema :DistrictInput do
      allOf do
        schema do
          key :required, [:district]

          property :district do
            key :title, :district
            key :type, :object
            key :required, [:name, :city_id, :country_id]

            property :name do
              key :type, :string
            end

            property :active do
              key :type, :boolean
            end

            property :city_id do
              key :type, :integer
              key :format, :int32
            end

            property :country_id do
              key :type, :integer
              key :format, :int32
            end
          end
        end
      end
    end
  end
end
