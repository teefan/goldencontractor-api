# frozen_string_literal: true
# rubocop:disable BlockLength, ModuleLength
module JobSchemaDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_schema :Job do
      key :title, :Job
      key :required, [:id, :title, :description, :land_width, :land_length, :land_area, :floor_width, :floor_length,
                      :floor_area, :number_of_floors, :has_terrace, :has_mezzanine, :has_basement, :planned_start_date,
                      :estimated_cost, :budget_level, :budget, :verified, :published_at, :construction_service_id,
                      :construction_type_id, :workflow_status, :next_processing_date, :max_contractors, :address,
                      :country_id, :city_id, :district_id, :user_id]

      property :id do
        key :type, :integer
        key :format, :int32
      end

      property :title do
        key :type, :string
      end

      property :description do
        key :type, :string
      end

      property :land_width do
        key :type, :number
        key :format, :float
      end

      property :land_length do
        key :type, :number
        key :format, :float
      end

      property :land_area do
        key :type, :number
        key :format, :float
      end

      property :floor_width do
        key :type, :number
        key :format, :float
      end

      property :floor_length do
        key :type, :number
        key :format, :float
      end

      property :floor_area do
        key :type, :number
        key :format, :float
      end

      property :number_of_floors do
        key :type, :integer
        key :format, :int32
      end

      property :has_terrace do
        key :type, :boolean
      end

      property :has_mezzanine do
        key :type, :boolean
      end

      property :has_basement do
        key :type, :boolean
      end

      property :planned_start_date do
        key :type, :string
        key :format, :date
      end

      property :estimated_cost do
        key :type, :number
        key :format, :double
      end

      property :budget_level do
        key :type, :integer
        key :format, :int32
      end

      property :budget do
        key :type, :number
        key :format, :double
      end

      property :verified do
        key :type, :boolean
      end

      property :published_at do
        key :type, :string
        key :format, :date
      end

      property :construction_service_id do
        key :type, :integer
        key :format, :int32
      end

      property :construction_type_id do
        key :type, :integer
        key :format, :int32
      end

      property :next_processing_date do
        key :type, :string
        key :format, :date
      end

      property :max_contractors do
        key :type, :integer
        key :format, :int32
      end

      property :address do
        key :type, :string
      end

      property :workflow_status do
        key :type, :string
      end

      property :country_id do
        key :type, :integer
        key :format, :int32
      end

      property :city_id do
        key :type, :integer
        key :format, :int32
      end

      property :district_id do
        key :type, :integer
        key :format, :int32
      end

      property :user_id do
        key :type, :integer
        key :format, :int32
      end
    end

    swagger_schema :JobInput do
      allOf do
        schema do
          key :required, [:job]

          property :job do
            key :title, :job
            key :type, :object
            key :required, [:title]

            property :title do
              key :type, :string
            end

            property :description do
              key :type, :string
            end

            property :land_width do
              key :type, :number
              key :format, :float
            end

            property :land_length do
              key :type, :number
              key :format, :float
            end

            property :land_area do
              key :type, :number
              key :format, :float
            end

            property :floor_width do
              key :type, :number
              key :format, :float
            end

            property :floor_length do
              key :type, :number
              key :format, :float
            end

            property :floor_area do
              key :type, :number
              key :format, :float
            end

            property :number_of_floors do
              key :type, :integer
              key :format, :int32
            end

            property :has_terrace do
              key :type, :boolean
            end

            property :has_mezzanine do
              key :type, :boolean
            end

            property :has_basement do
              key :type, :boolean
            end

            property :planned_start_date do
              key :type, :string
              key :format, :date
            end

            property :estimated_cost do
              key :type, :number
              key :format, :double
            end

            property :budget_level do
              key :type, :integer
              key :format, :int32
            end

            property :budget do
              key :type, :number
              key :format, :double
            end

            property :verified do
              key :type, :boolean
            end

            property :published_at do
              key :type, :string
              key :format, :date
            end

            property :construction_service_id do
              key :type, :integer
              key :format, :int32
            end

            property :construction_type_id do
              key :type, :integer
              key :format, :int32
            end

            property :next_processing_date do
              key :type, :string
              key :format, :date
            end

            property :max_contractors do
              key :type, :integer
              key :format, :int32
            end

            property :address do
              key :type, :string
            end

            property :flow_action do
              key :type, :string
            end

            property :country_id do
              key :type, :integer
              key :format, :int32
            end

            property :city_id do
              key :type, :integer
              key :format, :int32
            end

            property :district_id do
              key :type, :integer
              key :format, :int32
            end

            property :user_id do
              key :type, :integer
              key :format, :int32
            end

            property :attachment_ids do
              key :type, :array
              items do
                key :type, :integer
                key :format, :int32
              end
            end
          end
        end
      end
    end
  end
end
