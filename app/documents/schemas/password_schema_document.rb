# frozen_string_literal: true
# rubocop:disable BlockLength
module PasswordSchemaDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_schema :PasswordInput do
      allOf do
        schema do
          key :required, [:user]

          property :user do
            key :title, :user
            key :type, :object
            key :required, [:email]

            property :email do
              key :type, :string
            end
          end
        end
      end
    end

    swagger_schema :PasswordEditInput do
      allOf do
        schema do
          key :required, [:user]

          property :user do
            key :title, :user
            key :type, :object
            key :required, [:reset_password_token, :password]

            property :reset_password_token do
              key :type, :string
            end

            property :password do
              key :type, :string
              key :format, :password
            end

            property :password_confirmation do
              key :type, :string
              key :format, :password
            end
          end
        end
      end
    end
  end
end
