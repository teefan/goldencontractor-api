# frozen_string_literal: true
# rubocop:disable BlockLength
module RegistrationSchemaDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_schema :RegistrationInput do
      allOf do
        schema do
          key :required, [:user]

          property :user do
            key :title, :user
            key :type, :object
            key :required, [:email, :password]

            property :email do
              key :type, :string
            end

            property :password do
              key :type, :string
              key :format, :password
              key :description, 'New password'
            end

            property :password_confirmation do
              key :type, :string
              key :format, :password
            end

            property :current_password do
              key :type, :string
              key :format, :password
              key :description, 'Required current password when updating account'
            end

            property :user_profile_attributes do
              key :type, :object
              key '$ref', :UserProfileAttributesInput
            end

            property :company_attributes do
              key :type, :object
              key '$ref', :CompanyAttributesInput
            end
          end
        end
      end
    end
  end
end
