# frozen_string_literal: true
# rubocop:disable BlockLength
module RoleSchemaDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_schema :Role do
      key :title, :Role
      key :required, [:id, :name, :resource_type, :resource_id]

      property :id do
        key :type, :integer
        key :format, :int32
      end

      property :name do
        key :type, :string
      end

      property :resource_type do
        key :type, :string
      end

      property :resource_id do
        key :type, :integer
        key :format, :int32
      end
    end

    swagger_schema :RoleInput do
      allOf do
        schema do
          key :required, [:role]

          property :role do
            key :title, :role
            key :type, :object
            key :required, [:name]

            property :name do
              key :type, :string
            end

            property :resource_type do
              key :type, :string
            end

            property :resource_id do
              key :type, :integer
              key :format, :int32
            end
          end
        end
      end
    end
  end
end
