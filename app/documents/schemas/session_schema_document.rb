# frozen_string_literal: true
module SessionSchemaDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_schema :SessionInput do
      allOf do
        schema do
          key :required, [:user]

          property :user do
            key :title, :user
            key :type, :object
            key :required, [:email, :password]

            property :email do
              key :type, :string
            end

            property :password do
              key :type, :string
              key :format, :password
            end
          end
        end
      end
    end
  end
end
