# frozen_string_literal: true
# rubocop:disable BlockLength, ModuleLength
module UserProfileSchemaDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_schema :UserProfile do
      key :title, :UserProfile
      key :required, [:name, :gender, :phone_number, :street_address, :avatar, :country_id, :city_id, :district_id]

      property :name do
        key :type, :string
      end

      property :gender do
        key :type, :string
      end

      property :date_of_birth do
        key :type, :string
        key :format, :date
      end

      property :phone_number do
        key :type, :string
      end

      property :contact_time do
        key :type, :string
      end

      property :street_address do
        key :type, :string
      end

      property :avatar do
        key :type, :file
      end

      property :verified_contractor do
        key :type, :boolean
      end

      property :country_id do
        key :type, :integer
        key :format, :int32
      end

      property :city_id do
        key :type, :integer
        key :format, :int32
      end

      property :district_id do
        key :type, :integer
        key :format, :int32
      end
    end

    swagger_schema :UserProfileInput do
      allOf do
        schema do
          key :required, [:user_profile]

          property :user_profile do
            key :title, :user_profile
            key :type, :object
            key :required, [:name, :gender, :phone_number]

            property :id do
              key :type, :integer
              key :format, :int32
            end

            property :name do
              key :type, :string
            end

            property :gender do
              key :type, :string
            end

            property :date_of_birth do
              key :type, :string
              key :format, :date
            end

            property :phone_number do
              key :type, :string
            end

            property :street_address do
              key :type, :string
            end

            property :avatar do
              key :type, :file
            end

            property :contact_time do
              key :type, :string
            end

            property :verified_contractor do
              key :type, :boolean
            end

            property :country_id do
              key :type, :integer
              key :format, :int32
            end

            property :city_id do
              key :type, :integer
              key :format, :int32
            end

            property :district_id do
              key :type, :integer
              key :format, :int32
            end
          end
        end
      end
    end

    swagger_schema :UserProfileAttributesInput do
      allOf do
        schema do
          key :required, [:name, :gender, :phone_number]

          property :name do
            key :type, :string
          end

          property :gender do
            key :type, :string
          end

          property :date_of_birth do
            key :type, :string
            key :format, :date
          end

          property :phone_number do
            key :type, :string
          end

          property :street_address do
            key :type, :string
          end

          property :avatar do
            key :type, :file
          end

          property :contact_time do
            key :type, :string
          end

          property :verified_contractor do
            key :type, :boolean
          end

          property :country_id do
            key :type, :integer
            key :format, :int32
          end

          property :city_id do
            key :type, :integer
            key :format, :int32
          end

          property :district_id do
            key :type, :integer
            key :format, :int32
          end
        end
      end
    end
  end
end
