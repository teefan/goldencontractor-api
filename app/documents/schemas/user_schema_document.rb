# frozen_string_literal: true
module UserSchemaDocument
  extend ActiveSupport::Concern
  included do
    include Swagger::Blocks

    swagger_schema :User do
      key :title, :User
      key :required, [:id, :email, :password]

      property :id do
        key :type, :integer
        key :format, :int32
      end

      property :email do
        key :type, :string
      end

      property :user_profile do
        key :type, :object
        key '$ref', :UserProfile
      end

      property :company do
        key :type, :object
        key '$ref', :Company
      end
    end
  end
end
