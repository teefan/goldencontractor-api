# frozen_string_literal: true
class ApplicationMailer < ActionMailer::Base
  default from: 'email@nhathauvang.com'
  layout 'mailer'
end
