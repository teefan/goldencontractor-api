# frozen_string_literal: true
# == Schema Information
#
# Table name: architects
#
#  id           :integer          not null, primary key
#  name         :string           not null
#  description  :text
#  email        :string
#  phone_number :string
#  address      :string
#  country_id   :integer
#  city_id      :integer
#  district_id  :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_architects_on_city_id       (city_id)
#  index_architects_on_country_id    (country_id)
#  index_architects_on_district_id   (district_id)
#  index_architects_on_email         (email)
#  index_architects_on_name          (name)
#  index_architects_on_phone_number  (phone_number)
#
# Foreign Keys
#
#  fk_rails_14a24e6925  (district_id => districts.id)
#  fk_rails_af934ec308  (city_id => cities.id)
#  fk_rails_c21c7e0dcb  (country_id => countries.id)
#

class Architect < ApplicationRecord
  include ArchitectSchemaDocument
  include Filterable

  validates :name, presence: true

  belongs_to :country, required: false
  belongs_to :city, required: false
  belongs_to :district, required: false

  scope :by_email, ->(email) { where(email: email) }
  scope :by_country_id, ->(country_id) { where(country_id: country_id) }
  scope :by_city_id, ->(city_id) { where(city_id: city_id) }
  scope :by_district_id, ->(district_id) { where(district_id: district_id) }
end
