# frozen_string_literal: true
# == Schema Information
#
# Table name: attachments
#
#  id              :integer          not null, primary key
#  file            :string
#  file_size       :float
#  content_type    :string
#  attachable_id   :integer
#  attachable_type :string
#  user_id         :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_attachments_on_attachable_type_and_attachable_id  (attachable_type,attachable_id)
#  index_attachments_on_user_id                            (user_id)
#
# Foreign Keys
#
#  fk_rails_5650a5e7db  (user_id => users.id)
#

class Attachment < ApplicationRecord
  attr_accessor :files

  include AttachmentSchemaDocument
  include Filterable

  # validates :file, presence: true

  belongs_to :attachable, polymorphic: true, required: false
  belongs_to :user, required: false

  scope :by_attachable_id, ->(attachable_id) { where(attachable_id: attachable_id) }
  scope :by_attachable_type, ->(attachable_type) { where(attachable_type: attachable_type) }
  scope :by_user_id, ->(user_id) { where(user_id: user_id) }

  mount_uploader :file, FileUploader
end
