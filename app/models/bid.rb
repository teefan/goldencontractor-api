# frozen_string_literal: true
# == Schema Information
#
# Table name: bids
#
#  id         :integer          not null, primary key
#  job_id     :integer
#  user_id    :integer
#  status     :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_bids_on_job_id   (job_id)
#  index_bids_on_status   (status)
#  index_bids_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_29ea1b3ce2  (job_id => jobs.id)
#  fk_rails_e173de2ed3  (user_id => users.id)
#

class Bid < ApplicationRecord
  attr_accessor :flow_action

  include BidSchemaDocument
  include BidFlow
  include Filterable

  belongs_to :job, required: false
  belongs_to :user, required: false

  scope :by_job_id, ->(job_id) { where(job_id: job_id) }
  scope :by_user_id, ->(user_id) { where(user_id: user_id) }
end
