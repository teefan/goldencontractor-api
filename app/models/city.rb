# frozen_string_literal: true
# == Schema Information
#
# Table name: cities
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  active     :boolean          default(TRUE)
#  country_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_cities_on_active      (active)
#  index_cities_on_country_id  (country_id)
#  index_cities_on_name        (name)
#
# Foreign Keys
#
#  fk_rails_996e05be41  (country_id => countries.id)
#

class City < ApplicationRecord
  include CitySchemaDocument
  include Filterable

  validates :name, presence: true

  belongs_to :country

  has_many :districts, dependent: :destroy

  has_many :user_profiles, dependent: :nullify
  has_many :companies, dependent: :nullify
  has_many :architects, dependent: :nullify
  has_many :jobs, dependent: :nullify

  scope :by_country_id, ->(country_id) { where(country_id: country_id) }
end
