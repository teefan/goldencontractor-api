# frozen_string_literal: true
# == Schema Information
#
# Table name: companies
#
#  id             :integer          not null, primary key
#  name           :string
#  tax_number     :string
#  street_address :string
#  website_url    :string
#  country_id     :integer
#  city_id        :integer
#  district_id    :integer
#  user_id        :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  avatar         :string
#
# Indexes
#
#  index_companies_on_city_id      (city_id)
#  index_companies_on_country_id   (country_id)
#  index_companies_on_district_id  (district_id)
#  index_companies_on_name         (name)
#  index_companies_on_tax_number   (tax_number)
#  index_companies_on_user_id      (user_id)
#
# Foreign Keys
#
#  fk_rails_268726594d  (city_id => cities.id)
#  fk_rails_75b15a5c36  (country_id => countries.id)
#  fk_rails_daaa2a7e61  (district_id => districts.id)
#  fk_rails_e57cb42012  (user_id => users.id)
#

class Company < ApplicationRecord
  include CompanySchemaDocument
  include Filterable

  validates :name, presence: true
  validates :tax_number, presence: true

  belongs_to :country, required: false
  belongs_to :city, required: false
  belongs_to :district, required: false
  belongs_to :user, required: false

  scope :by_tax_number, ->(tax_number) { where(tax_number: tax_number) }
  scope :by_country_id, ->(country_id) { where(country_id: country_id) }
  scope :by_city_id, ->(city_id) { where(city_id: city_id) }
  scope :by_district_id, ->(district_id) { where(district_id: district_id) }
  scope :by_user_id, ->(user_id) { where(user_id: user_id) }

  resourcify

  mount_uploader :avatar, AvatarUploader

  def ensure_owner_presence(current_user)
    self.user_id = current_user.id if user_id.blank?
  end
end
