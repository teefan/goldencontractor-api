# frozen_string_literal: true
module AttachFile
  extend ActiveSupport::Concern

  included do
    attr_accessor :attachment_ids

    # attach pre-uploaded files
    after_save :attach_files

    # attach list of files by ID to the job
    def attach_files
      return unless attachment_ids.is_a?(Array)
      Attachment
        .where(id: attachment_ids, attachable_id: nil, attachable_type: nil)
        .update_all(attachable_id: id, attachable_type: self.class.name)
    end
  end
end
