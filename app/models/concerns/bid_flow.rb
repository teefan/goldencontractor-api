# frozen_string_literal: true
require 'active_support/concern'

module BidFlow
  extend ActiveSupport::Concern

  included do
    include AASM

    VALID_BID_FLOWS = %w(accept reject cancel).freeze

    aasm column: :status do
      state :proposed, initial: true
      state :accepted
      state :rejected
      state :cancelled

      event :accept do
        transitions to: :accepted
      end

      event :reject do
        transitions to: :rejected
      end

      event :cancel do
        transitions to: :cancelled
      end
    end
  end
end
