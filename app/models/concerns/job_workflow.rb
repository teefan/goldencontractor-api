# frozen_string_literal: true
require 'active_support/concern'

# rubocop:disable BlockLength
module JobWorkflow
  extend ActiveSupport::Concern

  included do
    include AASM

    VALID_WORKFLOWS = %w(approve check close remind_later open verify arrange_first_meeting arrange_meeting
                         assign_contractor negotiate start_bidding sign finish).freeze

    aasm column: :workflow_status do
      state :draft, initial: true
      state :new
      state :calling_to_verify
      state :closed
      state :pending
      state :verified
      state :opened
      state :assigning_contractor
      state :bidding
      state :scheduling_first_meeting
      state :scheduling_meeting
      state :negotiating
      state :signed
      state :done

      event :approve do
        transitions to: :new
      end

      event :check do
        transitions to: :calling_to_verify
      end

      event :close do
        transitions to: :closed
      end

      event :remind_later do
        transitions to: :pending
      end

      event :open do
        transitions to: :opened
      end

      event :verify do
        transitions to: :verified
      end

      event :arrange_first_meeting do
        transitions to: :scheduling_first_meeting
      end

      event :arrange_meeting do
        transitions to: :scheduling_meeting
      end

      event :assign_contractor do
        transitions to: :assigning_contractor
      end

      event :negotiate do
        transitions to: :negotiating
      end

      event :start_bidding do
        transitions to: :bidding
      end

      event :sign do
        transitions to: :signed
      end

      event :finish do
        transitions to: :done
      end
    end
  end
end
