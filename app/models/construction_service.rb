# frozen_string_literal: true
# == Schema Information
#
# Table name: construction_services
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  active     :boolean          default(TRUE)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_construction_services_on_active  (active)
#  index_construction_services_on_name    (name)
#

class ConstructionService < ApplicationRecord
  include ConstructionServiceSchemaDocument

  validates :name, presence: true

  has_many :jobs, dependent: :nullify
end
