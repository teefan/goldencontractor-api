# frozen_string_literal: true
# == Schema Information
#
# Table name: construction_types
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  active     :boolean          default(TRUE)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_construction_types_on_active  (active)
#  index_construction_types_on_name    (name)
#

class ConstructionType < ApplicationRecord
  include ConstructionTypeSchemaDocument

  validates :name, presence: true

  has_many :jobs, dependent: :nullify
end
