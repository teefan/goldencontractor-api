# frozen_string_literal: true
# == Schema Information
#
# Table name: countries
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  alpha2     :string           not null
#  active     :boolean          default(TRUE)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_countries_on_active  (active)
#  index_countries_on_alpha2  (alpha2)
#  index_countries_on_name    (name)
#

class Country < ApplicationRecord
  include CountrySchemaDocument
  include Filterable

  validates :name, presence: true, uniqueness: true
  validates :alpha2, presence: true

  has_many :cities, dependent: :destroy
  has_many :districts, dependent: :destroy

  has_many :user_profiles, dependent: :nullify
  has_many :companies, dependent: :nullify
  has_many :architects, dependent: :nullify
  has_many :jobs, dependent: :nullify

  scope :by_alpha2, ->(alpha2) { where(alpha2: alpha2) }
end
