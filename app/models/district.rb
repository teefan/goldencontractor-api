# frozen_string_literal: true
# == Schema Information
#
# Table name: districts
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  active     :boolean          default(TRUE)
#  city_id    :integer
#  country_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_districts_on_active      (active)
#  index_districts_on_city_id     (city_id)
#  index_districts_on_country_id  (country_id)
#  index_districts_on_name        (name)
#
# Foreign Keys
#
#  fk_rails_8245f4596b  (country_id => countries.id)
#  fk_rails_92c48f7cf2  (city_id => cities.id)
#

class District < ApplicationRecord
  include DistrictSchemaDocument
  include Filterable

  validates :name, presence: true

  belongs_to :city
  belongs_to :country

  has_many :user_profiles, dependent: :nullify
  has_many :companies, dependent: :nullify
  has_many :architects, dependent: :nullify
  has_many :jobs, dependent: :nullify

  scope :by_country_id, ->(country_id) { where(country_id: country_id) }
  scope :by_city_id, ->(city_id) { where(city_id: city_id) }
end
