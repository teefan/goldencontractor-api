# frozen_string_literal: true
# == Schema Information
#
# Table name: jobs
#
#  id                      :integer          not null, primary key
#  title                   :string
#  description             :text
#  land_width              :float
#  land_length             :float
#  land_area               :float
#  floor_width             :float
#  floor_length            :float
#  floor_area              :float
#  number_of_floors        :integer
#  has_terrace             :boolean
#  has_mezzanine           :boolean
#  has_basement            :boolean
#  planned_start_date      :date
#  estimated_cost          :decimal(16, 2)
#  budget_level            :integer
#  budget                  :decimal(16, 2)
#  verified                :boolean
#  published_at            :date
#  construction_service_id :integer
#  construction_type_id    :integer
#  workflow_status         :string
#  next_processing_date    :date
#  max_contractors         :integer
#  address                 :string
#  country_id              :integer
#  city_id                 :integer
#  district_id             :integer
#  user_id                 :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#
# Indexes
#
#  index_jobs_on_budget                   (budget)
#  index_jobs_on_city_id                  (city_id)
#  index_jobs_on_construction_service_id  (construction_service_id)
#  index_jobs_on_construction_type_id     (construction_type_id)
#  index_jobs_on_country_id               (country_id)
#  index_jobs_on_district_id              (district_id)
#  index_jobs_on_floor_area               (floor_area)
#  index_jobs_on_floor_length             (floor_length)
#  index_jobs_on_floor_width              (floor_width)
#  index_jobs_on_max_contractors          (max_contractors)
#  index_jobs_on_next_processing_date     (next_processing_date)
#  index_jobs_on_number_of_floors         (number_of_floors)
#  index_jobs_on_published_at             (published_at)
#  index_jobs_on_title                    (title)
#  index_jobs_on_user_id                  (user_id)
#  index_jobs_on_verified                 (verified)
#  index_jobs_on_workflow_status          (workflow_status)
#
# Foreign Keys
#
#  fk_rails_01fd9c0583  (country_id => countries.id)
#  fk_rails_14f0f4a5d7  (construction_service_id => construction_services.id)
#  fk_rails_54d543406a  (city_id => cities.id)
#  fk_rails_6fc6b41c3e  (district_id => districts.id)
#  fk_rails_7a9254a5e8  (construction_type_id => construction_types.id)
#  fk_rails_df6238c8a6  (user_id => users.id)
#

class Job < ApplicationRecord
  attr_accessor :flow_action

  include JobSchemaDocument
  include JobWorkflow
  include Filterable
  include AttachFile

  validates :description, presence: true
  validates :land_area, presence: true
  validates :land_length, presence: true
  validates :land_width, presence: true
  validates :budget, presence: true
  # validates :number_of_floors, presence: true
  validates :construction_service_id, presence: true
  validates :construction_type_id, presence: true
  validates :city_id, presence: true
  validates :district_id, presence: true

  belongs_to :construction_service, required: false
  belongs_to :construction_type, required: false
  belongs_to :country, required: false
  belongs_to :city, required: false
  belongs_to :district, required: false
  belongs_to :user, required: false

  has_many :attachments, as: :attachable, dependent: :nullify
  has_many :bids, dependent: :destroy

  scope :by_construction_service_id,
        ->(construction_service_id) { where(construction_service_id: construction_service_id) }
  scope :by_construction_type_id, ->(construction_type_id) { where(construction_type_id: construction_type_id) }
  scope :by_country_id, ->(country_id) { where(country_id: country_id) }
  scope :by_city_id, ->(city_id) { where(city_id: city_id) }
  scope :by_district_id, ->(district_id) { where(district_id: district_id) }
  scope :by_user_id, ->(user_id) { where(user_id: user_id) }

  before_save :update_workflow

  def update_workflow
    return unless respond_to?("may_#{flow_action}?")
    send(flow_action)
  end
end
