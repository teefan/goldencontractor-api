# frozen_string_literal: true
# == Schema Information
#
# Table name: roles
#
#  id            :integer          not null, primary key
#  name          :string
#  resource_type :string
#  resource_id   :integer
#  created_at    :datetime
#  updated_at    :datetime
#
# Indexes
#
#  index_roles_on_name                                    (name)
#  index_roles_on_name_and_resource_type_and_resource_id  (name,resource_type,resource_id)
#

class Role < ApplicationRecord
  include RoleSchemaDocument

  validates :resource_type, inclusion: { in: Rolify.resource_types }, allow_nil: true

  belongs_to :resource, polymorphic: true, optional: true

  has_and_belongs_to_many :users, join_table: 'users_roles' # rubocop:disable HasAndBelongsToMany

  scopify
end
