# frozen_string_literal: true
# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string
#  failed_attempts        :integer          default(0), not null
#  unlock_token           :string
#  locked_at              :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_users_on_confirmation_token    (confirmation_token) UNIQUE
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_unlock_token          (unlock_token) UNIQUE
#

class User < ApplicationRecord
  include UserSchemaDocument
  include AuthenticatedUserSchemaDocument
  include RegistrationSchemaDocument
  include SessionSchemaDocument
  include PasswordSchemaDocument
  include Filterable

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable

  validates :user_profile, presence: true

  validates_associated :user_profile
  validates_associated :company

  has_one :user_profile, dependent: :destroy
  has_one :company, dependent: :destroy
  has_many :jobs, dependent: :nullify
  has_many :bids, dependent: :destroy

  scope :by_email, ->(email) { where(email: email) }

  accepts_nested_attributes_for :user_profile
  accepts_nested_attributes_for :company

  rolify

  after_create :assign_default_role

  def assign_default_role
    add_role(:member) if roles.blank?
    add_role(:contractor) if company.present?
  end

  def contractor?
    has_role? :contractor
  end

  # return user JWT authentication token
  def auth_token
    JsonWebToken.encode(id: id, email: email)
  end

  def self.create_user(user_params)
    return if user_params.blank?

    user_params = user_params.permit(:id, :email, :password, user_profile_attributes:
      [:id, :name, :date_of_birth, :phone_number, :street_address, :contact_time])

    existing_user = find_by(email: user_params[:email])
    if existing_user
      existing_user.update(user_params)
    else
      create(user_params)
    end
  end
end
