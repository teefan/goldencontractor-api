# frozen_string_literal: true
# == Schema Information
#
# Table name: user_profiles
#
#  id                  :integer          not null, primary key
#  name                :string
#  gender              :string
#  date_of_birth       :date
#  phone_number        :string
#  street_address      :string
#  contact_time        :string
#  avatar              :string
#  country_id          :integer
#  city_id             :integer
#  district_id         :integer
#  user_id             :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  verified_contractor :boolean
#
# Indexes
#
#  index_user_profiles_on_city_id              (city_id)
#  index_user_profiles_on_country_id           (country_id)
#  index_user_profiles_on_district_id          (district_id)
#  index_user_profiles_on_name_and_gender      (name,gender)
#  index_user_profiles_on_phone_number         (phone_number)
#  index_user_profiles_on_street_address       (street_address)
#  index_user_profiles_on_user_id              (user_id)
#  index_user_profiles_on_verified_contractor  (verified_contractor)
#
# Foreign Keys
#
#  fk_rails_2950e3cad0  (country_id => countries.id)
#  fk_rails_46160e0086  (district_id => districts.id)
#  fk_rails_87a6352e58  (user_id => users.id)
#  fk_rails_fc4f14b651  (city_id => cities.id)
#

class UserProfile < ApplicationRecord
  include UserProfileSchemaDocument

  validates :name, presence: true, length: { minimum: 2 }
  validates :gender, inclusion: { in: %w(male female) }, on: :update, allow_blank: true
  # validates :phone_number, presence: true

  belongs_to :country, required: false
  belongs_to :city, required: false
  belongs_to :district, required: false
  belongs_to :user, required: false

  mount_uploader :avatar, AvatarUploader
end
