# frozen_string_literal: true
class AttachmentPolicy < ApplicationPolicy
  attr_reader :user, :attachment

  def initialize(user, attachment)
    @user = user
    @attachment = attachment
  end

  def update?
    attachment.user_id == user.id || user.has_role?(:admin)
  end

  def destroy?
    attachment.user_id == user.id || user.has_role?(:admin)
  end

  class Scope < Scope
    def resolve
      scope
    end
  end
end
