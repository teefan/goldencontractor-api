# frozen_string_literal: true
class BidPolicy < ApplicationPolicy
  attr_reader :user, :bid

  def initialize(user, bid)
    @user = user
    @bid = bid
  end

  def index?
    user.has_role?(:contractor) || user.has_role?(:admin)
  end

  def create?
    user.has_role?(:contractor) || user.has_role?(:admin)
  end

  def update?
    bid.user_id == user.id || bid.job.user_id == user.id || user.has_role?(:admin)
  end

  def destroy?
    bid.user_id == user.id || bid.job.user_id == user.id || user.has_role?(:admin)
  end

  class Scope < Scope
    def resolve
      scope
    end
  end
end
