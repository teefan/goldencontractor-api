# frozen_string_literal: true
class CityPolicy < ApplicationPolicy
  attr_reader :user, :city

  def initialize(user, city)
    @user = user
    @city = city
  end

  def create?
    user.has_role?(:admin)
  end

  def update?
    user.has_role?(:admin)
  end

  def destroy?
    user.has_role?(:admin)
  end

  class Scope < Scope
    def resolve
      scope
    end
  end
end
