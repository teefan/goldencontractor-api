# frozen_string_literal: true
class CountryPolicy < ApplicationPolicy
  attr_reader :user, :country

  def initialize(user, country)
    @user = user
    @country = country
  end

  def create?
    user.has_role?(:admin)
  end

  def update?
    user.has_role?(:admin)
  end

  def destroy?
    user.has_role?(:admin)
  end

  class Scope < Scope
    def resolve
      scope
    end
  end
end
