# frozen_string_literal: true
class DistrictPolicy < ApplicationPolicy
  attr_reader :user, :district

  def initialize(user, district)
    @user = user
    @district = district
  end

  def create?
    user.has_role?(:admin)
  end

  def update?
    user.has_role?(:admin)
  end

  def destroy?
    user.has_role?(:admin)
  end

  class Scope < Scope
    def resolve
      scope
    end
  end
end
