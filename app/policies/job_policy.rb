# frozen_string_literal: true
class JobPolicy < ApplicationPolicy
  attr_reader :user, :job

  def initialize(user, job)
    @user = user
    @job = job
  end

  def update?
    job.user_id == user.id || user.has_role?(:admin)
  end

  def destroy?
    user.has_role?(:admin)
  end

  class Scope < Scope
    def resolve
      scope
    end
  end
end
