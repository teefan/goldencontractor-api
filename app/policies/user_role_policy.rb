# frozen_string_literal: true
class UserRolePolicy < ApplicationPolicy
  attr_reader :user, :role_user

  def initialize(user, role_user)
    @user = user
    @role_user = role_user
  end

  def index?
    user.has_role?(:admin)
  end

  def create?
    user.has_role?(:admin)
  end

  def update?
    user.has_role?(:admin)
  end

  def destroy?
    user.has_role?(:admin)
  end

  class Scope < Scope
    def resolve
      scope
    end
  end
end
