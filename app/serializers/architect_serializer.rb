# frozen_string_literal: true
class ArchitectSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :email, :phone_number, :address, :district_id, :city_id, :country_id

  # has_one :country
  # has_one :city
  # has_one :district
end
