# frozen_string_literal: true
class AttachmentSerializer < ActiveModel::Serializer
  attributes :id, :file, :file_size, :content_type, :attachable_type, :attachable_id, :user_id

  attribute :name do
    file? ? File.basename(object.file.url) : nil
  end

  def file?
    object.file&.url ? true : false
  end

  # has_one :attachable
  # has_one :user
end
