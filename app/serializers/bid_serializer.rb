# frozen_string_literal: true
class BidSerializer < ActiveModel::Serializer
  attributes :id, :status, :job_id, :user_id

  # has_one :job
  # has_one :user
end
