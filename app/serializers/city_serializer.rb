# frozen_string_literal: true
class CitySerializer < ActiveModel::Serializer
  attributes :id, :name, :active, :country_id

  # has_one :country
end
