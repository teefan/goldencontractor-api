# frozen_string_literal: true
class CompanySerializer < ActiveModel::Serializer
  attributes :id, :name, :tax_number, :street_address, :website_url, :avatar, :district_id, :city_id, :country_id,
             :user_id

  # has_one :country
  # has_one :city
  # has_one :district
  # has_one :user
end
