# frozen_string_literal: true
class ConstructionServiceSerializer < ActiveModel::Serializer
  attributes :id, :name, :active
end
