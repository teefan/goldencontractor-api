# frozen_string_literal: true
class ConstructionTypeSerializer < ActiveModel::Serializer
  attributes :id, :name, :active
end
