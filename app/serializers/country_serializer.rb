# frozen_string_literal: true
class CountrySerializer < ActiveModel::Serializer
  attributes :id, :name, :alpha2, :active
end
