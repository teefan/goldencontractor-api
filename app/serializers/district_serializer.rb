# frozen_string_literal: true
class DistrictSerializer < ActiveModel::Serializer
  attributes :id, :name, :active, :city_id, :country_id

  # has_one :country
  # has_one :city
end
