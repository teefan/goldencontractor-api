# frozen_string_literal: true
class JobSerializer < ActiveModel::Serializer
  attributes :id, :title, :description, :land_width, :land_length, :land_area, :floor_width, :floor_length, :floor_area,
             :number_of_floors, :has_terrace, :has_mezzanine, :has_basement, :planned_start_date, :estimated_cost,
             :budget_level, :budget, :verified, :published_at, :workflow_status, :next_processing_date,
             :max_contractors, :address, :construction_service_id, :construction_type_id, :district_id, :city_id,
             :country_id, :user_id

  has_one :construction_service
  has_one :construction_type
  # has_one :country
  has_one :city
  has_one :district
  # has_one :user
  has_many :attachments
end
