# frozen_string_literal: true
class UserProfileSerializer < ActiveModel::Serializer
  attributes :id, :name, :gender, :avatar, :contact_time, :district_id, :city_id, :country_id, :user_id

  attribute :date_of_birth, if: :owner_or_admin?
  attribute :phone_number, if: :owner_or_admin?
  attribute :street_address, if: :owner_or_admin?
  attribute :verified_contractor, if: :owner_or_admin?

  # has_one :user

  def owner_or_admin?
    true if scope.present? && (scope.has_role?(:admin) || scope.id == object.user_id)
  end
end
