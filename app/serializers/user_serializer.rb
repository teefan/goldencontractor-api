# frozen_string_literal: true
class UserSerializer < ActiveModel::Serializer
  attributes :id

  attribute :email, if: :owner_or_admin?
  attribute :auth_token, if: :owner_or_admin?
  attribute :is_contractor do
    object.contractor?
  end

  has_one :user_profile
  has_one :company

  def admin?
    scope.present? && scope.has_role?(:admin) ? true : false
  end

  def owner_or_admin?
    scope.present? && (scope.has_role?(:admin) || scope.id == object.id) ? true : false
  end
end
