# frozen_string_literal: true
server 'beta-api.nhathauvang.com', user: 'deploy', roles: %w(app db web)

set :ssh_options,
    keys: %w(~/.ssh/id_rsa),
    forward_agent: true,
    auth_methods: %w(publickey password)

set :branch, :beta
