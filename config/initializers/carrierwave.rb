# frozen_string_literal: true
require 'carrierwave/storage/abstract'
require 'carrierwave/storage/file'
require 'carrierwave/storage/fog'

if Rails.env.development?
  CarrierWave.configure do |config|
    config.storage = :file
  end
end

if Rails.env.test?
  CarrierWave.configure do |config|
    config.storage = :file
    config.enable_processing = false
  end
end

if Rails.env.production?
  CarrierWave.configure do |config|
    config.storage = :fog
    config.fog_provider = 'fog/aws'                        # required
    config.fog_credentials = {
      provider:              'AWS',                        # required
      aws_access_key_id:     ENV['AWS_ACCESS_KEY_ID'],     # required
      aws_secret_access_key: ENV['AWS_SECRET_ACCESS_KEY'], # required
      region:                ENV['FOG_REGION'],            # optional, defaults to 'us-east-1'
      host:                  ENV['FOG_HOST'],              # optional, defaults to nil
      endpoint:              ENV['FOG_ENDPOINT']           # optional, defaults to nil
    }
    config.fog_directory  = ENV['FOR_DIRECTORY']                            # required
    config.fog_public     = true                                            # optional, defaults to true
    config.fog_attributes = { 'Cache-Control': "max-age=#{365.days.to_i}" } # optional, defaults to {}
  end
end
