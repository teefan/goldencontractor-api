require 'sidekiq/web'

# frozen_string_literal: true
# rubocop:disable BlockLength
Rails.application.routes.draw do
  # Requires header "Accept: application/json;version=1"
  scope module: :v1, defaults: { format: :json }, constraints: VersionConstraint.new(version: 1) do
    get '/me' => 'users#me'
    resources :users, only: [:index, :show]

    get '/user_profile' => 'user_profiles#index', as: :user_profile
    get '/user_profiles/:id' => 'user_profiles#show', as: :user_profiles
    resource :user_profile, except: [:show]

    resources :roles, except: [:show]

    get '/user_roles' => 'user_roles#index', as: :user_roles
    resource :user_roles, except: [:show, :update]

    resources :companies

    resources :construction_services

    resources :construction_types

    resources :architects

    resources :jobs

    resources :attachments

    resource :workflow, controller: :workflow, only: [:show, :update]

    resources :bids

    devise_for :users, controllers: {
      sessions: 'v1/users/sessions',
      registrations: 'v1/users/registrations',
      passwords: 'v1/users/passwords',
      confirmations: 'v1/users/confirmations',
      unlocks: 'v1/users/unlocks'
    }

    resources :countries

    resources :cities

    resources :districts
  end

  resources :api_doc, only: [:index]

  mount Sidekiq::Web => '/sidekiq'
end
