class CreateCountries < ActiveRecord::Migration[5.0]
  def change
    create_table :countries do |t|
      t.string :name, null: false
      t.string :alpha2, null: false
      t.boolean :active, default: true

      t.timestamps
    end

    add_index :countries, :name
    add_index :countries, :alpha2
    add_index :countries, :active
  end
end
