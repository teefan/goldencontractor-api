class CreateCities < ActiveRecord::Migration[5.0]
  def change
    create_table :cities do |t|
      t.string :name, null: false
      t.boolean :active, default: true
      t.references :country, foreign_key: true

      t.timestamps
    end

    add_index :cities, :name
    add_index :cities, :active
  end
end
