class CreateDistricts < ActiveRecord::Migration[5.0]
  def change
    create_table :districts do |t|
      t.string :name, null: false
      t.boolean :active, default: true
      t.references :city, foreign_key: true
      t.references :country, foreign_key: true

      t.timestamps
    end

    add_index :districts, :name
    add_index :districts, :active
  end
end
