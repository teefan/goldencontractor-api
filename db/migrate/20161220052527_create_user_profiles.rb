class CreateUserProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :user_profiles do |t|
      t.string :name
      t.string :gender
      t.date :date_of_birth
      t.string :phone_number
      t.string :street_address
      t.string :contact_time
      t.string :avatar
      t.references :country, foreign_key: true
      t.references :city, foreign_key: true
      t.references :district, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end

    add_index :user_profiles, [:name, :gender]
    add_index :user_profiles, :phone_number
    add_index :user_profiles, :street_address
  end
end
