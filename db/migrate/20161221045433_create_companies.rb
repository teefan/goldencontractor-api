class CreateCompanies < ActiveRecord::Migration[5.0]
  def change
    create_table :companies do |t|
      t.string :name
      t.string :tax_number
      t.string :street_address
      t.string :website_url
      t.references :country, foreign_key: true
      t.references :city, foreign_key: true
      t.references :district, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end

    add_index :companies, :name
    add_index :companies, :tax_number
  end
end
