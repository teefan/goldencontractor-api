class CreateConstructionServices < ActiveRecord::Migration[5.0]
  def change
    create_table :construction_services do |t|
      t.string :name, null: false
      t.boolean :active, default: true

      t.timestamps
    end

    add_index :construction_services, :name
    add_index :construction_services, :active
  end
end
