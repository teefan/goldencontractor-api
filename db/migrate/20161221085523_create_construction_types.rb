class CreateConstructionTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :construction_types do |t|
      t.string :name, null: false
      t.boolean :active, default: true

      t.timestamps
    end

    add_index :construction_types, :name
    add_index :construction_types, :active
  end
end
