class AddVerifiedContractorToUserProfiles < ActiveRecord::Migration[5.0]
  def change
    add_column :user_profiles, :verified_contractor, :boolean

    add_index :user_profiles, :verified_contractor
  end
end
