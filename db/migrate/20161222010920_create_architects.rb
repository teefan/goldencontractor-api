class CreateArchitects < ActiveRecord::Migration[5.0]
  def change
    create_table :architects do |t|
      t.string :name, null: false
      t.text :description
      t.string :email
      t.string :phone_number
      t.string :address
      t.references :country, foreign_key: true
      t.references :city, foreign_key: true
      t.references :district, foreign_key: true

      t.timestamps
    end

    add_index :architects, :name
    add_index :architects, :email
    add_index :architects, :phone_number
  end
end
