class CreateJobs < ActiveRecord::Migration[5.0]
  def change # rubocop:disable MethodLength, AbcSize
    create_table :jobs do |t| # rubocop:disable BlockLength
      t.string :title
      t.text :description
      t.float :land_width
      t.float :land_length
      t.float :land_area
      t.float :floor_width
      t.float :floor_length
      t.float :floor_area
      t.integer :number_of_floors
      t.boolean :has_terrace
      t.boolean :has_mezzanine
      t.boolean :has_basement
      t.date :planned_start_date
      t.decimal :estimated_cost, precision: 16, scale: 2
      t.integer :budget_level
      t.decimal :budget, precision: 16, scale: 2
      t.boolean :verified
      t.date :published_at
      t.references :construction_service, foreign_key: true
      t.references :construction_type, foreign_key: true
      t.string :workflow_status
      t.date :next_processing_date
      t.integer :max_contractors
      t.string :address
      t.references :country, foreign_key: true
      t.references :city, foreign_key: true
      t.references :district, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end

    add_index :jobs, :title
    add_index :jobs, :floor_width
    add_index :jobs, :floor_length
    add_index :jobs, :floor_area
    add_index :jobs, :number_of_floors
    add_index :jobs, :budget
    add_index :jobs, :verified
    add_index :jobs, :published_at
    add_index :jobs, :workflow_status
    add_index :jobs, :next_processing_date
    add_index :jobs, :max_contractors
  end
end
