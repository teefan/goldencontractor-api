class CreateAttachments < ActiveRecord::Migration[5.0]
  def change
    create_table :attachments do |t|
      t.string :file
      t.float :file_size
      t.string :content_type
      t.references :attachable, polymorphic: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
