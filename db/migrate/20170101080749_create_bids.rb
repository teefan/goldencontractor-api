class CreateBids < ActiveRecord::Migration[5.0]
  def change
    create_table :bids do |t|
      t.references :job, foreign_key: true
      t.references :user, foreign_key: true
      t.string :status

      t.timestamps
    end

    add_index :bids, :status
  end
end
