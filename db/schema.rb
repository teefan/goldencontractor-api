# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170101080749) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "architects", force: :cascade do |t|
    t.string   "name",         null: false
    t.text     "description"
    t.string   "email"
    t.string   "phone_number"
    t.string   "address"
    t.integer  "country_id"
    t.integer  "city_id"
    t.integer  "district_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["city_id"], name: "index_architects_on_city_id", using: :btree
    t.index ["country_id"], name: "index_architects_on_country_id", using: :btree
    t.index ["district_id"], name: "index_architects_on_district_id", using: :btree
    t.index ["email"], name: "index_architects_on_email", using: :btree
    t.index ["name"], name: "index_architects_on_name", using: :btree
    t.index ["phone_number"], name: "index_architects_on_phone_number", using: :btree
  end

  create_table "attachments", force: :cascade do |t|
    t.string   "file"
    t.float    "file_size"
    t.string   "content_type"
    t.string   "attachable_type"
    t.integer  "attachable_id"
    t.integer  "user_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["attachable_type", "attachable_id"], name: "index_attachments_on_attachable_type_and_attachable_id", using: :btree
    t.index ["user_id"], name: "index_attachments_on_user_id", using: :btree
  end

  create_table "bids", force: :cascade do |t|
    t.integer  "job_id"
    t.integer  "user_id"
    t.string   "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["job_id"], name: "index_bids_on_job_id", using: :btree
    t.index ["status"], name: "index_bids_on_status", using: :btree
    t.index ["user_id"], name: "index_bids_on_user_id", using: :btree
  end

  create_table "cities", force: :cascade do |t|
    t.string   "name",                      null: false
    t.boolean  "active",     default: true
    t.integer  "country_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["active"], name: "index_cities_on_active", using: :btree
    t.index ["country_id"], name: "index_cities_on_country_id", using: :btree
    t.index ["name"], name: "index_cities_on_name", using: :btree
  end

  create_table "companies", force: :cascade do |t|
    t.string   "name"
    t.string   "tax_number"
    t.string   "street_address"
    t.string   "website_url"
    t.integer  "country_id"
    t.integer  "city_id"
    t.integer  "district_id"
    t.integer  "user_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "avatar"
    t.index ["city_id"], name: "index_companies_on_city_id", using: :btree
    t.index ["country_id"], name: "index_companies_on_country_id", using: :btree
    t.index ["district_id"], name: "index_companies_on_district_id", using: :btree
    t.index ["name"], name: "index_companies_on_name", using: :btree
    t.index ["tax_number"], name: "index_companies_on_tax_number", using: :btree
    t.index ["user_id"], name: "index_companies_on_user_id", using: :btree
  end

  create_table "construction_services", force: :cascade do |t|
    t.string   "name",                      null: false
    t.boolean  "active",     default: true
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["active"], name: "index_construction_services_on_active", using: :btree
    t.index ["name"], name: "index_construction_services_on_name", using: :btree
  end

  create_table "construction_types", force: :cascade do |t|
    t.string   "name",                      null: false
    t.boolean  "active",     default: true
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["active"], name: "index_construction_types_on_active", using: :btree
    t.index ["name"], name: "index_construction_types_on_name", using: :btree
  end

  create_table "countries", force: :cascade do |t|
    t.string   "name",                      null: false
    t.string   "alpha2",                    null: false
    t.boolean  "active",     default: true
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["active"], name: "index_countries_on_active", using: :btree
    t.index ["alpha2"], name: "index_countries_on_alpha2", using: :btree
    t.index ["name"], name: "index_countries_on_name", using: :btree
  end

  create_table "districts", force: :cascade do |t|
    t.string   "name",                      null: false
    t.boolean  "active",     default: true
    t.integer  "city_id"
    t.integer  "country_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["active"], name: "index_districts_on_active", using: :btree
    t.index ["city_id"], name: "index_districts_on_city_id", using: :btree
    t.index ["country_id"], name: "index_districts_on_country_id", using: :btree
    t.index ["name"], name: "index_districts_on_name", using: :btree
  end

  create_table "jobs", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.float    "land_width"
    t.float    "land_length"
    t.float    "land_area"
    t.float    "floor_width"
    t.float    "floor_length"
    t.float    "floor_area"
    t.integer  "number_of_floors"
    t.boolean  "has_terrace"
    t.boolean  "has_mezzanine"
    t.boolean  "has_basement"
    t.date     "planned_start_date"
    t.decimal  "estimated_cost",          precision: 16, scale: 2
    t.integer  "budget_level"
    t.decimal  "budget",                  precision: 16, scale: 2
    t.boolean  "verified"
    t.date     "published_at"
    t.integer  "construction_service_id"
    t.integer  "construction_type_id"
    t.string   "workflow_status"
    t.date     "next_processing_date"
    t.integer  "max_contractors"
    t.string   "address"
    t.integer  "country_id"
    t.integer  "city_id"
    t.integer  "district_id"
    t.integer  "user_id"
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.index ["budget"], name: "index_jobs_on_budget", using: :btree
    t.index ["city_id"], name: "index_jobs_on_city_id", using: :btree
    t.index ["construction_service_id"], name: "index_jobs_on_construction_service_id", using: :btree
    t.index ["construction_type_id"], name: "index_jobs_on_construction_type_id", using: :btree
    t.index ["country_id"], name: "index_jobs_on_country_id", using: :btree
    t.index ["district_id"], name: "index_jobs_on_district_id", using: :btree
    t.index ["floor_area"], name: "index_jobs_on_floor_area", using: :btree
    t.index ["floor_length"], name: "index_jobs_on_floor_length", using: :btree
    t.index ["floor_width"], name: "index_jobs_on_floor_width", using: :btree
    t.index ["max_contractors"], name: "index_jobs_on_max_contractors", using: :btree
    t.index ["next_processing_date"], name: "index_jobs_on_next_processing_date", using: :btree
    t.index ["number_of_floors"], name: "index_jobs_on_number_of_floors", using: :btree
    t.index ["published_at"], name: "index_jobs_on_published_at", using: :btree
    t.index ["title"], name: "index_jobs_on_title", using: :btree
    t.index ["user_id"], name: "index_jobs_on_user_id", using: :btree
    t.index ["verified"], name: "index_jobs_on_verified", using: :btree
    t.index ["workflow_status"], name: "index_jobs_on_workflow_status", using: :btree
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.string   "resource_type"
    t.integer  "resource_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
    t.index ["name"], name: "index_roles_on_name", using: :btree
  end

  create_table "user_profiles", force: :cascade do |t|
    t.string   "name"
    t.string   "gender"
    t.date     "date_of_birth"
    t.string   "phone_number"
    t.string   "street_address"
    t.string   "contact_time"
    t.string   "avatar"
    t.integer  "country_id"
    t.integer  "city_id"
    t.integer  "district_id"
    t.integer  "user_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.boolean  "verified_contractor"
    t.index ["city_id"], name: "index_user_profiles_on_city_id", using: :btree
    t.index ["country_id"], name: "index_user_profiles_on_country_id", using: :btree
    t.index ["district_id"], name: "index_user_profiles_on_district_id", using: :btree
    t.index ["name", "gender"], name: "index_user_profiles_on_name_and_gender", using: :btree
    t.index ["phone_number"], name: "index_user_profiles_on_phone_number", using: :btree
    t.index ["street_address"], name: "index_user_profiles_on_street_address", using: :btree
    t.index ["user_id"], name: "index_user_profiles_on_user_id", using: :btree
    t.index ["verified_contractor"], name: "index_user_profiles_on_verified_contractor", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree
  end

  add_foreign_key "architects", "cities"
  add_foreign_key "architects", "countries"
  add_foreign_key "architects", "districts"
  add_foreign_key "attachments", "users"
  add_foreign_key "bids", "jobs"
  add_foreign_key "bids", "users"
  add_foreign_key "cities", "countries"
  add_foreign_key "companies", "cities"
  add_foreign_key "companies", "countries"
  add_foreign_key "companies", "districts"
  add_foreign_key "companies", "users"
  add_foreign_key "districts", "cities"
  add_foreign_key "districts", "countries"
  add_foreign_key "jobs", "cities"
  add_foreign_key "jobs", "construction_services"
  add_foreign_key "jobs", "construction_types"
  add_foreign_key "jobs", "countries"
  add_foreign_key "jobs", "districts"
  add_foreign_key "jobs", "users"
  add_foreign_key "user_profiles", "cities"
  add_foreign_key "user_profiles", "countries"
  add_foreign_key "user_profiles", "districts"
  add_foreign_key "user_profiles", "users"
end
