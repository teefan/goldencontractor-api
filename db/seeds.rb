# frozen_string_literal: true
# Default roles
Role.where(name: 'member').first_or_create
Role.where(name: 'admin').first_or_create
Role.where(name: 'contractor').first_or_create

# Root user
root_user = User.find_by(email: ENV.fetch('ROOT_EMAIL'))

if root_user.nil?
  root_user = User.create!(
    email: ENV.fetch('ROOT_EMAIL'),
    password: ENV.fetch('ROOT_PASSWORD')
  )
end

root_user.add_role(:admin)

# Default construction services
seed_construction_services = [
  'Thiết kế - Thi công',
  'Thiết kế kiến trúc',
  'Thi công xây dựng',
  'Hoàn thiện nội thất',
  'Xin phép xây dựng',
  'Cải tạo nhà cửa',
  'Quản lý dự án'
]

seed_construction_services.each do |construction_service_name|
  ConstructionService.where(name: construction_service_name).first_or_create
end

# Default construction types
seed_construction_types = [
  'Nhà phố',
  'Biệt thự',
  'Căn hộ',
  'Văn phòng',
  'Cafe, Shop, Nhà hàng',
  'Khách sạn, Resort'
]

seed_construction_types.each do |construction_type_name|
  ConstructionType.where(name: construction_type_name).first_or_create
end

country = Country.where(id: 1, name: 'Việt Nam', alpha2: 'vn').first_or_create

provinces = IO.readlines(Rails.root.join('db', 'seeds', 'vn_provinces.csv'))
districts = IO.readlines(Rails.root.join('db', 'seeds', 'vn_districts.csv'))

provinces.each do |province|
  province_data = province.split(',')
  city = country.cities.where(id: province_data[0].to_i, name: province_data[1]).first_or_create

  districts.each do |district|
    district_data = district.split(',')
    next if district_data[3].to_i != city.id

    city.districts.where(id: district_data[0].to_i, name: district_data[1], country_id: country.id).first_or_create
  end
end
