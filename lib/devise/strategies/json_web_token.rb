# frozen_string_literal: true
require 'json_web_token'
require 'devise/strategies/authenticatable'

module Devise
  module Strategies
    class JsonWebToken < Authenticatable
      def valid?
        request.headers['Authorization'].present?
      end

      def authenticate!
        fail! && return if claims.nil?

        resource = User.find_by(id: claims.fetch('id'))
        if resource
          remember_me resource
          success! resource
        else
          fail!
        end
      end

      private

      def claims
        auth_header = request.headers['Authorization']
        auth_components = auth_header.split(' ')

        return nil if auth_components.first != 'Bearer'

        token = auth_components.last
        auth_header && token && ::JsonWebToken.decode(token)
      rescue
        nil
      end
    end
  end
end

# We already add it in Devise initializer file
# Warden::Strategies.add(:json_web_token, Devise::Strategies::JsonWebToken)
