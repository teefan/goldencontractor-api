# frozen_string_literal: true
class JsonWebToken
  class << self
    def encode(payload, exp = 24.hours.from_now)
      payload[:exp] = exp.to_i
      JWT.encode(payload, Rails.application.secrets.secret_key_base)
    end

    def decode(token)
      HashWithIndifferentAccess.new(JWT.decode(token, Rails.application.secrets.secret_key_base).first)
    rescue JWT::DecodeError => e
      HashWithIndifferentAccess.new(error: e.message)
    end
  end
end
