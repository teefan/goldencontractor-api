# frozen_string_literal: true
namespace :data do
  desc 'Generate fake users'
  task generate_users: :environment do
    1000.times.each do
      User.create(email: FFaker::Internet.email, password: '12345678')
    end
  end
end
