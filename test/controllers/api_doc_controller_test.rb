# frozen_string_literal: true
require 'test_helper'

class APIDocControllerTest < ActionDispatch::IntegrationTest
  test 'should get index' do
    get api_doc_index_url, as: :json
    assert_response :success
  end
end
