# frozen_string_literal: true
require 'test_helper'

module V1
  class ArchitectsControllerTest < ActionDispatch::IntegrationTest
    setup do
      @architect = architects(:tran_doan)

      @user = users(:teefan)

      @country = countries(:viet_nam)
      @city = cities(:tphcm)
    end

    test 'should get index' do
      get architects_url, headers: auth_headers(@user), as: :json
      assert_response :success
    end

    test 'should create architect' do
      assert_difference('Architect.count') do
        post architects_url, headers: auth_headers(@user), params: {
          architect: {
            name: 'Architect Tran',
            description: 'Cool architect description',
            email: 'kts.tran@example.com',
            phone_number: '456789123',
            address: '456 Nguyen Trai',
            country_id: @country.id,
            city_id: @city.id
          }
        }, as: :json
      end

      assert_response 201
    end

    test 'should show architect' do
      get architect_url(@architect), headers: auth_headers(@user), as: :json
      assert_response :success
    end

    test 'should update architect' do
      patch architect_url(@architect), headers: auth_headers(@user), params: {
        architect: {
          name: 'Updated Architect Tran',
          description: 'Awesome architect description',
          email: 'kts.tran@example.com',
          phone_number: '456789123-123',
          address: '456 Nguyen Trai',
          country_id: @country.id,
          city_id: @city.id
        }
      }, as: :json

      assert_response 204
    end

    test 'should destroy architect' do
      assert_difference('Architect.count', -1) do
        delete architect_url(@architect), headers: auth_headers(@user), as: :json
      end

      assert_response 204
    end
  end
end
