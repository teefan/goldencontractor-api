# frozen_string_literal: true
require 'test_helper'

module V1
  class AttachmentsControllerTest < ActionDispatch::IntegrationTest
    setup do
      @job = jobs(:house_building)
      @attachment = attachments(:job_image)

      @user = users(:teefan)
      @user.add_role(:admin)

      @file_path = Rails.root.join('test', 'fixtures', 'files', 'uploaded_file.txt')
    end

    test 'should get index' do
      get attachments_url, headers: headers, as: :json
      assert_response :success
    end

    test 'should create attachment' do
      assert_difference('Attachment.count') do
        post attachments_url, headers: headers, params: {
          attachment: {
            file: Rack::Test::UploadedFile.new(@file_path, 'text/plain'),
            attachable_id: @job.id,
            attachable_type: @job.class.name,
            user_id: @user.id
          }
        }, as: :json
      end

      assert_response 201
    end

    # test 'should create multiple attachments' do
    #   assert_difference('Attachment.count', 2) do
    #     post attachments_url, headers: headers, params: {
    #       attachment: {
    #         files: [Rack::Test::UploadedFile.new(@file_path, 'text/plain'),
    #                 Rack::Test::UploadedFile.new(@file_path, 'text/plain'),
    #                 Rack::Test::UploadedFile.new(@file_path, 'text/plain')],
    #         attachable_id: @job.id,
    #         attachable_type: @job.class.name,
    #         user_id: @user.id
    #       }
    #     }, as: :json
    #   end
    #
    #   assert_response 201
    # end

    test 'should show attachment' do
      get attachment_url(@attachment), headers: headers, as: :json
      assert_response :success
    end

    test 'should update attachment' do
      patch attachment_url(@attachment), headers: auth_headers(@user), params: {
        attachment: {
          file: Rack::Test::UploadedFile.new(@file_path, 'text/plain'),
          attachable_id: @job.id,
          attachable_type: @job.class.name,
          user_id: @user.id
        }
      }, as: :json

      assert_response 204
    end

    test 'should destroy attachment' do
      assert_difference('Attachment.count', -1) do
        delete attachment_url(@attachment), headers: auth_headers(@user), as: :json
      end

      assert_response 204
    end
  end
end
