# frozen_string_literal: true
require 'test_helper'

module V1
  class BidsControllerTest < ActionDispatch::IntegrationTest
    setup do
      @job = jobs(:house_building)
      @bid = bids(:house_building_bid)

      @contractor = users(:contractor)
    end

    test 'should get index' do
      get bids_url, headers: auth_headers(@contractor), as: :json
      assert_response :success
    end

    test 'should create bid' do
      assert_difference('Bid.count') do
        post bids_url, headers: auth_headers(@contractor), params: {
          bid: {
            job_id: @job.id,
            user_id: @contractor.id
          }
        }, as: :json
      end

      assert_response 201
    end

    test 'should show bid' do
      get bid_url(@bid), headers: auth_headers(@contractor), as: :json
      assert_response :success
    end

    test 'should update bid' do
      patch bid_url(@bid), headers: auth_headers(@contractor), params: {
        bid: {
          job_id: @job.id,
          user_id: @contractor.id
        }
      }, as: :json

      assert_response 204
    end

    test 'should destroy bid' do
      assert_difference('Bid.count', -1) do
        delete bid_url(@bid), headers: auth_headers(@contractor), as: :json
      end

      assert_response 204
    end
  end
end
