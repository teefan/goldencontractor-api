# frozen_string_literal: true
require 'test_helper'

module V1
  class CitiesControllerTest < ActionDispatch::IntegrationTest
    setup do
      @country = countries(:viet_nam)
      @city = cities(:tphcm)

      @user = users(:teefan)
      @user.add_role(:admin)
    end

    test 'should get index' do
      get cities_url, headers: headers, as: :json
      assert_response :success
    end

    test 'should create city' do
      assert_difference('City.count') do
        post cities_url, headers: auth_headers(@user), params: {
          city: {
            name: 'My City',
            active: true,
            country_id: @country.id
          }
        }, as: :json
      end

      assert_response 201
    end

    test 'should show city' do
      get city_url(@city), headers: headers, as: :json
      assert_response :success
    end

    test 'should update city' do
      patch city_url(@city), headers: auth_headers(@user), params: {
        city: {
          name: 'My Updated City',
          active: true,
          country_id: @country.id
        }
      }, as: :json

      assert_response 204
    end

    test 'should destroy city' do
      assert_difference('City.count', -1) do
        delete city_url(@city), headers: auth_headers(@user), as: :json
      end

      assert_response 204
    end
  end
end
