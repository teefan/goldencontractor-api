# frozen_string_literal: true
require 'test_helper'

module V1
  class CompaniesControllerTest < ActionDispatch::IntegrationTest
    setup do
      @company = companies(:teefan_inc)

      @country = countries(:viet_nam)
      @city = cities(:tphcm)
      @district = districts(:district_1)

      @user = users(:teefan)
    end

    test 'should get index' do
      get companies_url, headers: headers, as: :json
      assert_response :success
    end

    test 'should create company' do
      assert_difference('Company.count') do
        post companies_url, headers: auth_headers(@user), params: {
          company: {
            name: 'My Company',
            tax_number: '123456789',
            street_address: '123 Nguyen Hue',
            website_url: 'http://www.my-company.com',
            country_id: @country.id,
            city_id: @city.id,
            district_id: @district.id,
            user_id: @user.id
          }
        }, as: :json
      end

      assert_response 201
    end

    test 'should show company' do
      get company_url(@company), headers: headers, as: :json
      assert_response :success
    end

    test 'should update company' do
      patch company_url(@company), headers: auth_headers(@user), params: {
        company: {
          name: 'My Company',
          tax_number: '123456789',
          street_address: '123 Nguyen Hue',
          website_url: 'http://www.my-company.com',
          country_id: @country.id,
          city_id: @city.id,
          district_id: @district.id,
          user_id: @user.id
        }
      }, as: :json

      assert_response 204
    end

    test 'should destroy company' do
      assert_difference('Company.count', -1) do
        delete company_url(@company), headers: auth_headers(@user), as: :json
      end

      assert_response 204
    end
  end
end
