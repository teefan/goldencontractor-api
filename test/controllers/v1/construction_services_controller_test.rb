# frozen_string_literal: true
require 'test_helper'

module V1
  class ConstructionServicesControllerTest < ActionDispatch::IntegrationTest
    setup do
      @construction_service = construction_services(:construct)

      @user = users(:teefan)
      @user.add_role(:admin)
    end

    test 'should get index' do
      get construction_services_url, headers: headers, as: :json
      assert_response :success
    end

    test 'should create construction_service' do
      assert_difference('ConstructionService.count') do
        post construction_services_url, headers: auth_headers(@user), params: {
          construction_service: {
            name: 'My Service',
            active: true
          }
        }, as: :json
      end

      assert_response 201
    end

    test 'should show construction_service' do
      get construction_service_url(@construction_service), headers: headers, as: :json
      assert_response :success
    end

    test 'should update construction_service' do
      patch construction_service_url(@construction_service), headers: auth_headers(@user), params: {
        construction_service: {
          name: 'My Updated Service',
          active: true
        }
      }, as: :json

      assert_response 204
    end

    test 'should destroy construction_service' do
      assert_difference('ConstructionService.count', -1) do
        delete construction_service_url(@construction_service), headers: auth_headers(@user), as: :json
      end

      assert_response 204
    end
  end
end
