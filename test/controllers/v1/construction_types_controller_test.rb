# frozen_string_literal: true
require 'test_helper'

module V1
  class ConstructionTypesControllerTest < ActionDispatch::IntegrationTest
    setup do
      @construction_type = construction_types(:house)

      @user = users(:teefan)
      @user.add_role(:admin)
    end

    test 'should get index' do
      get construction_types_url, headers: headers, as: :json
      assert_response :success
    end

    test 'should create construction_type' do
      assert_difference('ConstructionType.count') do
        post construction_types_url, headers: auth_headers(@user), params: {
          construction_type: {
            name: 'My Construction Type',
            active: true
          }
        }, as: :json
      end

      assert_response 201
    end

    test 'should show construction_type' do
      get construction_type_url(@construction_type), headers: headers, as: :json
      assert_response :success
    end

    test 'should update construction_type' do
      patch construction_type_url(@construction_type), headers: auth_headers(@user), params: {
        construction_type: {
          name: 'My Updated Construction Type',
          active: true
        }
      }, as: :json

      assert_response 204
    end

    test 'should destroy construction_type' do
      assert_difference('ConstructionType.count', -1) do
        delete construction_type_url(@construction_type), headers: auth_headers(@user), as: :json
      end

      assert_response 204
    end
  end
end
