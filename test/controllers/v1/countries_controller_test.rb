# frozen_string_literal: true
require 'test_helper'

module V1
  class CountriesControllerTest < ActionDispatch::IntegrationTest
    setup do
      @country = countries(:viet_nam)

      @user = users(:teefan)
      @user.add_role(:admin)
    end

    test 'should get index' do
      get countries_url, headers: headers, as: :json
      assert_response :success
    end

    test 'should create country' do
      assert_difference('Country.count') do
        post countries_url, headers: auth_headers(@user), params: {
          country: {
            name: 'My Country',
            alpha2: 'MC',
            active: true
          }
        }, as: :json
      end

      assert_response 201
    end

    test 'should show country' do
      get country_url(@country), headers: headers, as: :json
      assert_response :success
    end

    test 'should update country' do
      patch country_url(@country), headers: auth_headers(@user), params: {
        country: {
          name: 'My Updated Country',
          alpha2: 'MU',
          active: true
        }
      }, as: :json

      assert_response 204
    end

    test 'should destroy country' do
      assert_difference('Country.count', -1) do
        delete country_url(@country), headers: auth_headers(@user), as: :json
      end

      assert_response 204
    end
  end
end
