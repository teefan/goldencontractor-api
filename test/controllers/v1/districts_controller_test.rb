# frozen_string_literal: true
require 'test_helper'

module V1
  class DistrictsControllerTest < ActionDispatch::IntegrationTest
    setup do
      @district = districts(:district_1)
      @country = countries(:viet_nam)
      @city = cities(:tphcm)

      @user = users(:teefan)
      @user.add_role(:admin)
    end

    test 'should get index' do
      get districts_url, headers: headers, as: :json
      assert_response :success
    end

    test 'should create district' do
      assert_difference('District.count') do
        post districts_url, headers: auth_headers(@user), params: {
          district: {
            name: 'My District',
            active: true,
            city_id: @city.id,
            country_id: @country.id
          }
        }, as: :json
      end

      assert_response 201
    end

    test 'should show district' do
      get district_url(@district), headers: headers, as: :json
      assert_response :success
    end

    test 'should update district' do
      patch district_url(@district), headers: auth_headers(@user), params: {
        district: {
          name: 'My Updated District',
          active: true,
          city_id: @city.id,
          country_id: @country.id
        }
      }, as: :json

      assert_response 204
    end

    test 'should destroy district' do
      assert_difference('District.count', -1) do
        delete district_url(@district), headers: auth_headers(@user), as: :json
      end

      assert_response 204
    end
  end
end
