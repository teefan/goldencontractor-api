# frozen_string_literal: true
require 'test_helper'

module V1
  class JobsControllerTest < ActionDispatch::IntegrationTest
    setup do
      @job = jobs(:house_building)

      @country = countries(:viet_nam)
      @city = cities(:tphcm)
      @district = districts(:district_1)

      @construction_service = construction_services(:construct)
      @construction_type = construction_types(:house)

      @attachment = attachments(:job_image)

      @user = users(:teefan)
    end

    test 'should get index' do
      get jobs_url, headers: headers, as: :json
      assert_response :success
    end

    test 'should create job' do # rubocop:disable BlockLength
      assert_difference('Job.count') do # rubocop:disable BlockLength
        post jobs_url, headers: auth_headers(@user), params: {
          job: {
            title: 'My House',
            description: 'My House description',
            land_width: @job.land_width,
            land_length: @job.land_length,
            land_area: @job.land_area,
            floor_width: @job.floor_width,
            floor_length: @job.floor_length,
            floor_area: @job.floor_area,
            number_of_floors: @job.number_of_floors,
            has_terrace: @job.has_terrace,
            has_mezzanine: @job.has_mezzanine,
            has_basement: @job.has_basement,
            planned_start_date: @job.planned_start_date,
            estimated_cost: @job.estimated_cost,
            budget_level: @job.budget_level,
            budget: @job.budget,
            verified: @job.verified,
            published_at: @job.published_at,
            construction_service_id: @construction_service.id,
            construction_type_id: @construction_type.id,
            workflow_status: @job.workflow_status,
            next_processing_date: @job.next_processing_date,
            max_contractors: @job.max_contractors,
            address: @job.address,
            country_id: @country.id,
            city_id: @city.id,
            district_id: @district.id,
            user_id: @user.id,
            attachment_ids: [@attachment.id]
          }
        }, as: :json
      end

      assert_response 201
    end

    test 'should show job' do
      get job_url(@job), headers: headers, as: :json
      assert_response :success
    end

    test 'should update job' do # rubocop:disable BlockLength
      patch job_url(@job), headers: auth_headers(@user), params: {
        job: {
          title: 'My Updated House',
          description: 'My Updated House description',
          land_width: @job.land_width,
          land_length: @job.land_length,
          land_area: @job.land_area,
          floor_width: @job.floor_width,
          floor_length: @job.floor_length,
          floor_area: @job.floor_area,
          number_of_floors: @job.number_of_floors,
          has_terrace: @job.has_terrace,
          has_mezzanine: @job.has_mezzanine,
          has_basement: @job.has_basement,
          planned_start_date: @job.planned_start_date,
          estimated_cost: @job.estimated_cost,
          budget_level: @job.budget_level,
          budget: @job.budget,
          verified: @job.verified,
          published_at: @job.published_at,
          construction_service_id: @construction_service.id,
          construction_type_id: @construction_type.id,
          workflow_status: @job.workflow_status,
          next_processing_date: @job.next_processing_date,
          max_contractors: @job.max_contractors,
          address: @job.address,
          country_id: @country.id,
          city_id: @city.id,
          district_id: @district.id,
          user_id: @user.id,
          attachment_ids: [@attachment.id]
        }
      }, as: :json

      assert_response 204
    end

    test 'should destroy job' do
      assert_difference('Job.count', -1) do
        delete job_url(@job), headers: auth_headers(@user), as: :json
      end

      assert_response 204
    end
  end
end
