# frozen_string_literal: true
require 'test_helper'

module V1
  class RolesControllerTest < ActionDispatch::IntegrationTest
    setup do
      @role = roles(:admin)

      @user = users(:teefan)
      @user.add_role(:admin)
    end

    test 'should get index' do
      get roles_url, headers: auth_headers(@user), as: :json
      assert_response :success
    end

    test 'should create role' do
      assert_difference('Role.count') do
        post roles_url, headers: auth_headers(@user), params: {
          role: {
            name: :my_new_role
          }
        }, as: :json
      end

      assert_response 201
    end

    test 'should update role' do
      patch role_url(@role), headers: auth_headers(@user), params: {
        role: {
          name: :my_updated_new_role,
          resource_type: nil
        }
      }, as: :json

      assert_response 204
    end

    test 'should destroy role' do
      assert_difference('Role.count', -1) do
        delete role_url(@role), headers: auth_headers(@user), as: :json
      end

      assert_response 204
    end
  end
end
