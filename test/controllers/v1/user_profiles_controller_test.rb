# frozen_string_literal: true
require 'test_helper'

module V1
  class UserProfilesControllerTest < ActionDispatch::IntegrationTest
    setup do
      @country = countries(:viet_nam)
      @city = cities(:tphcm)
      @district = districts(:district_1)

      @user = users(:teefan)
    end

    test 'should get index' do
      get user_profile_url, headers: auth_headers(@user), as: :json
      assert_response :success
    end

    test 'should create user_profile' do
      UserProfile.destroy_all

      assert_difference('UserProfile.count') do
        post user_profile_url, headers: auth_headers(@user), params: {
          user_profile: {
            name: 'Long Tran',
            gender: 'male',
            date_of_birth: '1982-12-30',
            phone_number: '0123456789',
            street_address: '123 Nguyen Hue',
            country_id: @country.id,
            district_id: @district.id,
            city_id: @city.id
          }
        }, as: :json
      end

      assert_response 201
    end

    test 'should show user_profile' do
      @user_profile = user_profiles(:teefan_profile)

      get user_profiles_url(@user_profile), headers: auth_headers(@user), as: :json
      assert_response :success
    end

    test 'should update user_profile' do
      @user_profile = user_profiles(:teefan_profile)

      patch user_profile_url, headers: auth_headers(@user), params: {
        user_profile: {
          name: 'Long Teefan Tran',
          gender: 'male',
          date_of_birth: '1982-12-30',
          phone_number: '0123456789',
          street_address: '123 Nguyen Hue',
          country_id: @country.id,
          district_id: @district.id,
          city_id: @city.id
        }
      }, as: :json

      assert_response 204
    end

    test 'should destroy user_profile' do
      assert_difference('UserProfile.count', -1) do
        delete user_profile_url, headers: auth_headers(@user), as: :json
      end

      assert_response 204
    end
  end
end
