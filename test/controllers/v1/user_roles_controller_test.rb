# frozen_string_literal: true
require 'test_helper'

module V1
  class UserRolesControllerTest < ActionDispatch::IntegrationTest
    setup do
      @user = users(:teefan)
      @user.add_role(:admin)
    end

    test 'should get index' do
      get user_roles_url, headers: auth_headers(@user), as: :json
      assert_response :success
    end

    test 'should add user_role' do
      assert_difference('@user.roles.count') do
        post user_roles_url, headers: auth_headers(@user), params: {
          user_role: {
            name: :my_new_role
          }
        }, as: :json
      end

      assert_response 201
    end

    test 'should destroy user_role' do
      @user.add_role(:removing_role)

      assert_difference('@user.roles.count', -1) do
        delete user_roles_url, headers: auth_headers(@user), params: {
          user_role: {
            name: :removing_role
          }
        }, as: :json
      end

      assert_response 204
    end
  end
end
