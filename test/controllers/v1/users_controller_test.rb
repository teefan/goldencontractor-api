# frozen_string_literal: true
require 'test_helper'

module V1
  class UsersControllerTest < ActionDispatch::IntegrationTest
    setup do
      @user = users(:teefan)

      sign_in @user
    end

    test 'should get index' do
      get users_url, headers: headers, as: :json
      assert_response :success
    end

    test 'should show user' do
      get user_url(@user), headers: headers, as: :json
      assert_response :success
    end

    test 'should show me' do
      get me_url, headers: headers, as: :json
      assert_response :success
    end
  end
end
