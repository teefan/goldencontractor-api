# frozen_string_literal: true
require 'test_helper'

class JobTest < ActiveSupport::TestCase
  setup do
    @job = jobs(:house_building)
  end

  test 'update workflow' do
    @job.flow_action = :approve
    @job.update_workflow
    @job.save

    assert_equal(@job.workflow_status, 'new')
  end
end
