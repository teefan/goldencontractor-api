# frozen_string_literal: true
ENV['RAILS_ENV'] ||= 'test'

require 'simplecov'
SimpleCov.start 'test_frameworks' do
  add_filter '/bin/'
  add_filter '/config/'
  add_filter '/db/'
  add_filter '/documents/schemas'
  add_filter '/documents/paths'

  add_group 'Controllers', 'app/controllers'
  add_group 'Models', 'app/models'
  add_group 'Mailers', 'app/mailers'
  add_group 'Constraints', 'app/constraints'
  add_group 'Channels', 'app/channels'
  add_group 'Policies', 'app/policies'
  add_group 'Serializers', 'app/serializers'
  add_group 'Decorators', 'app/decorators'
  add_group 'Helpers', 'app/helpers'
  add_group 'Jobs', %w(app/jobs app/workers)
  add_group 'Libraries', 'lib'

  track_files '{app,lib}/**/*.rb'
end

require 'minitest/reporters'
Minitest::Reporters.use!

require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

module ActiveSupport
  class TestCase
    # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
    fixtures :all

    # Add more helper methods to be used by all tests here...
    def headers
      { Accept: 'application/json; version=1' }
    end

    def auth_headers(user)
      { Accept: 'application/json; version=1', Authorization: "Bearer #{user.auth_token}" }
    end
  end
end

module ActionDispatch
  class IntegrationTest
    include Devise::Test::IntegrationHelpers

    # called before every single test
    setup do
    end

    # called after every single test
    teardown do
      # when controller is using cache it may be a good idea to reset it afterwards
      Rails.cache.clear
    end
  end
end

module ActionController
  class TestCase
    include Devise::Test::ControllerHelpers
  end
end
